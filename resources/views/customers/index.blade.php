 @extends('layouts.index') @section('content-index')
<?php $menu = 'Customers' ?>
<div class="page-header">
    <h1>
        {{$menu}}
        <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      {{$menu}}
      </small>
    </h1>
</div>
@include('categories._session')
<div class="row">
    <div class="col-sm-12">
        <div class="pull-right" style="margin-bottom: 10px;">
            <button class="btn btn-sm btn-primary" onclick="showCreate();"><i class="ace-icon fa fa-plus bigger-120"></i>&nbsp;Create Customer</button>
        </div>
        <div id="form"></div>
    </div>

    <form method="post" action="{{ route('import-customer') }}" enctype="multipart/form-data" id="upload-customer">
        <div class="col-sm-10">
            {{ csrf_field() }}
            <input type="file" name="file" class="form-control">
        </div>
        <div class="col-sm-2">
            <div class="pull-right" style="width: 100%;">
                <button type="submit" class="btn btn-sm btn-primary" style="width: 100%;"><i class="ace-icon fa fa-upload bigger-120">&nbsp;</i>Upload</button>
            </div>
        </div>
    </form>
    <div class="col-sm-12"></div>
    <div class="col-sm-12">
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">Result {{ $menu }}</div>
        <div>
            <table id="customers-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nomor Customers</th>
                        <th>Name Customers</th>
                        <th>Email Customers</th>
                        <th>Address Customers</th>
                        <th>Phone Customers</th>
                        <th>Date Customers</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    jQuery(document).ready(function() {
        customers();
    });

    function customers() {
        jQuery(function($) {
            var t = $("#customers-table").DataTable({
                processing: true,
                serverSide: true,
                bAutoWidth: false,
                // dom: 'rti',
                ajax: "{{ route('customer-api') }}",
                search: {
                    caseInsensitive: true,
                },
                columns: [{
                    data: "id",
                    name: "id",
                }, {
                    data: "name",
                    name: "name",
                }, {
                    data: "email",
                    name: "email",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "address",
                    name: "address",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "phone",
                    name: "phone",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "created_at",
                    name: "created_at",
                    orderable: false,
                    searchable: false,
                    render: function(d) {
                        return moment(d).format("dddd, DD-MM-YYYY");
                    },
                }, {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                }, ],
                order: [
                    [0, "desc"]
                ],
                pageLength: 15,
                select: {
                    style: "multi",
                },
            });
        });
    }

    function autoRefreshData() {
        $("#customers-table").DataTable().ajax.reload();
    }

    setInterval("autoRefreshData()", 10000);


    function showCreate() {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/customer/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
                $('#upload-customer').attr("style", "display:none");
            },
        });
    }

    function showEdit(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/customer/edit" + '/' + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }
</script>
@endpush