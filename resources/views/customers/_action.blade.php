<span class="blue">
    <a class="blue" href="{{ url('web/customer/show', ['id' => $customer->id]) }}" onclick="">
        <i class="ace-icon fa fa-eye bigger-120"></i>
    </a>
</span>
{{-- <span class="green">
    <a class="green" href="" onclick="showEdit({{ $customer->id }})">
        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
    </a> --}}
</span>
<span class="red">
    <a class="red" href="{{ url('web/customer/delete', ['id' => $customer->id]) }}">
        <i class="ace-icon fa fa-trash-o bigger-120"></i>
    </a>
</span>