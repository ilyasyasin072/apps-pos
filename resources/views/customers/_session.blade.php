@if ($message = Session::get('success'))
<div class="widget-body">
    <div class="widget-main">
        <p class="alert alert-success">
            {{ $message }}
        </p>
    </div>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="widget-body">
    <div class="widget-main">
        <p class="alert alert-warning">
            {{ $message }}
        </p>
    </div>
</div>
@endif

@if ($message = Session::get('danger'))
<div class="widget-body">
    <div class="widget-main">
        <p class="alert alert-danger">
            {{ $message }}
        </p>
        <button class="close" data-dismiss="alert">
            <i class="ace-icon fa fa-times"></i>
        </button>
    </div>
</div>
@endif

@if($message = Session::get('info'))
<div class="alert alert-info">
    <button class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-hand-o-right"></i> Please note that demo server is not configured to save the changes,
    therefore you may see an error message.
</div>
@endif