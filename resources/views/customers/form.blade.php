<div class="row">
        <div class="">
            <!-- PAGE CONTENT BEGINS -->
            <div class="col-xs-12 col-sm-12">
                <div class="widget-box">
                    <div class="widget-header">
                        <?php $menus = 'Form Category'; ?>
                        <h4 class="widget-title">{{$menus}}</h4>

                        <span class="widget-toolbar">
                            <a href="#" data-action="settings">
                                <i class="ace-icon fa fa-cog"></i>
                            </a>
    
                            <a href="#" id="reload" data-action="reload">
                                <i class="ace-icon fa fa-refresh"></i>
                            </a>
    
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
    
                            <a href="#" data-action="close">
                                <i class="ace-icon fa fa-times"></i>
                            </a>
                        </span>
                    </div>
    
                    <div class="widget-body">
                        <div class="widget-main">
                            @if(!isset($customer))
                            <form class="form-horizontal" role="form" action="{{ route('customer-web-store') }}">
                                @else
                                <form class="form-horizontal" role="form" action="{{ route('customer-web-update', ['id' => $customer->id])}}">
                                        <div class="form-group">
                                                <label class="col-sm-5 control-label no-padding-right" for="form-field-2">Customer ID
                                                </label>
                                                <div class="col-sm-4">
                                                    <label class="control-label" for="form-field-2"></label>
                                                    <span class="help-inline col-xs-12 col-sm-7">
                                                        <span class="middle">
                                                            <h5>{{$customer->id}}</h5>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                    
                                    @endif
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right" for="form-field-2">Email</label>
                                        <div class="col-sm-4">
                                        <input type="email" id="form-field-2" name="email" value="{{ !isset($customer->email) ? '' : $customer->email }}" class="col-xs-10 col-sm-5" required />
                                        </div>
                                    </div>
    
                                    <div class="space-4"></div>
    
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right">
                                            Name
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text" name="name" class="col-xs-10 col-sm-5" id="form-input-readonly"  value="{{ !isset($customer->name) ? '' : $customer->name }} " required />
                                            <span class="help-inline col-xs-12 col-sm-7">
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="space-4"></div>
    
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right">
                                            Address
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text" name="address" class="col-xs-10 col-sm-5 form-control" id="form-input-readonly"   value="{{ !isset($customer->address) ? '' : $customer->address }} " required />
                                            <span class="help-inline col-xs-12 col-sm-7">
                                            </span>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>
    
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right">
                                            Phone
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="text" name="phone" class="col-xs-10 col-sm-5" id="form-input-readonly"  value="{{ !isset($customer->phone) ? '' : $customer->phone }} " required />
                                            <span class="help-inline col-xs-12 col-sm-7">
                                            </span>
                                        </div>
                                    </div>
                                    
    
                                    <div class="space-4"></div>
    
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-5 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Submit
                                            </button> &nbsp; &nbsp; &nbsp;
                                            <button class="btn" type="reset">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>