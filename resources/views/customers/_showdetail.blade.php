<div class="widget-body">
        <div class="widget-main padding-6 no-padding-left no-padding-right">
           <div class="main-content">
              <div class="main-content-inner">
                 <div class="page-content">
                    <div class="row">
                       <div class="col-xs-12">
                          <!-- PAGE CONTENT BEGINS -->
                          <div class="row">
                             <div class="col-xs-12">
                                <table id="simple-table" class="table  table-bordered table-hover">
                                   <thead>
                                      <tr>
                                         <th class="detail-col">Details</th>
                                         <th>Customer Name</th>
                                         <th>Customer Email</th>
                                         <th>Customer Phone</th>
                                         <th>Action</th>
                                      </tr>
                                   </thead>
                                   <tbody>
                                      <tr>
                                         <td class="center">
                                            <div class="action-buttons">
                                               <a href="#" class="green bigger-140 show-details-btn" title="Show Details">
                                               <i class="ace-icon fa fa-angle-double-down"></i>
                                               <span class="sr-only">Details</span>
                                               </a>
                                            </div>
                                         </td>
                                         <td>
                                            <a href="#" >{{ $customer->name }} </a>
                                            {{-- <span class="editable" id="pcs_spb" data-type="text" data-pk=" " data-url="" data-name="" data-emptytext="--- INPUT TOTAL ROLL KAIN ---">
                                            {{ $customer->name }}
                                            </span> --}}
                                         </td>
                                         <td>{{ $customer->email }}</td>
                                         <td>{{ $customer->phone }}</td>
                                         <td>
                                                    {{-- <span class="label label-xlg label-primary arrowed arrowed-right">
                                                        <a class="" >
                                                            <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                                        </a>
                                                    </span> --}}
                                                    <span class="label label-lg label-primary arrowed-right col-xs-12" href="" onclick="showEdit({{$customer->id}});" style="cursor: pointer;"> <i class="ace-icon fa fa-pencil-square-o bigger-120"></i></span>
                                         </td>
                                      </tr>
                                      <tr class="detail-row">
                                         <td colspan="8">
                                            <div class="table-detail">
                                               <div class="row">
                                                  <div class="col-xs-12 col-sm-2">
                                                     <div class="text-center">
                                                        <img height="150" class="thumbnail inline no-margin-bottom" alt="Domain Owner's Avatar" src="/ace-master/assets/images/avatars/profile-pic.jpg" />
                                                        <br />
                                                        <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                                           <div class="inline position-relative">
                                                              <a class="user-title-label" href="#">
                                                              <i class="ace-icon fa fa-circle light-green"></i>
                                                              &nbsp;
                                                              <span class="white">{{ $customer->name }}</span>
                                                              </a>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-7">
                                                     <div class="space visible-xs"></div>
                                                     <div class="profile-user-info profile-user-info-striped">
                                                        <div class="profile-info-row">
                                                           <div class="profile-info-name"> Username </div>
                                                           <div class="profile-info-value">
                                                              <span><a href="#" id="name" data-type="text" data-pk="{{ $customer->id }}" data-name="name" data-url='{{ route('customer-update', $customer->id) }}'>{{ $customer->name }}</a></span>
                                                             
                                                           </div>
                                                        </div>
                                                        <div class="profile-info-row">
                                                           <div class="profile-info-name"> Email </div>
                                                           <div class="profile-info-value">
                                                            <span><a href="#" id="email" data-type="text" data-pk="{{ $customer->id }}" data-name="email" data-url='{{ route('customer-update', $customer->id) }}'>{{ $customer->email }}</a></span>
                                                           </div>
                                                        </div>
                                                        <div class="profile-info-row">
                                                           <div class="profile-info-name"> Address </div>
                                                           <div class="profile-info-value">
                                                            <span><a href="#" id="address" data-type="textarea" data-pk="{{ $customer->id }}" data-name="address" data-url='{{ route('customer-update', $customer->id) }}'>{{ $customer->address }}</a></span>
                                                           </div>
                                                        </div>
                                                        <div class="profile-info-row">
                                                           <div class="profile-info-name"> Phone </div>
                                                           <div class="profile-info-value">
                                                            <span><a href="#" id="phone" data-type="text" data-pk="{{ $customer->id }}" data-name="phone" data-url='{{ route('customer-update', $customer->id) }}'>{{ $customer->phone }}</a></span>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                         </td>
                                      </tr>
                                   </tbody>
                                </table>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>

     @push('js')
<script type="text/javascript">
   function showEdit(id) {
          event.preventDefault();
          const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
   
          $.ajax({
              url: "/web/customer/edit" + '/' + id,
              type: "GET",
              data: {
                  CSRF_TOKEN,
              },
              success: function(data) {
                  // console.log(data);
                  $("#formupdate").html(data);
              },
          });
      }

      $('.show-details-btn').on('click', function(e) {
        e.preventDefault();
        $(this).closest('tr').next().toggleClass('open');
        $(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
         });
   
      // $('#email').editable({
      //    url: '/post',
      //    title: 'Enter Email'
      // });
      // $('#name').editable({
      //    url: '/post',
      //    title: 'Enter username'
      // });
      // $('#address').editable({
      //   url: '/post',
      //   title: 'Enter address',
      //   rows: 10
      // });
      // $('#phone').editable({
      //   url: '/post',
      //   title: 'Enter Phone',
      //   rows: 10
      // });
      // format custome
      $('#email').editable({
          type: 'text', 
          title: 'Enter username',
          url: '/post',
      //   validate: function(value) {
      //     var regex = /^[0-9]+$/;
      //     if(! regex.test(value)) {
      //         return 'numbers only!';
      //     }
      //   },
         display  : function(value) {
            if (!value) {
               $(this).empty();
               return;
            }
            console.log(value);
            var html = $('<div>').html('<a href="#">'+value+'</a>');
            $(this).html(html);
        }
        }); 

        $('#name').editable({
          type: 'text', 
          title: 'Enter username',
          url: '/post',
         display  : function(value) {
            if (!value) {
               $(this).empty();
               return;
            }
            console.log(value);
            var html = $('<div>').html('<a href="#">'+value+'</a>');
            $(this).html(html);
        }
        }); 

        $('#address').editable({
          type: 'text', 
          title: 'Enter username',
          url: '/post',
          rows: 10,
         display  : function(value) {
            if (!value) {
               $(this).empty();
               return;
            }
            console.log(value);
            var html = $('<div>').html('<a href="#">'+value+'</a>');
            $(this).html(html);
        }
        }); 

        $('#phone').editable({
          type: 'text', 
          title: 'Enter username',
          url: '/post',
          rows: 12,
        validate: function(value) {
          var regex = /^[0-9]+$/;
          if(! regex.test(value)) {
              return 'numbers only!';
          }
        },
         display  : function(value) {
            if (!value) {
               $(this).empty();
               return;
            }
            console.log(value);
            var html = $('<div>').html('<a href="#">'+value+'</a>');
            $(this).html(html);
        }
        }); 
</script>
@endpush