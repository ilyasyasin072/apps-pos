

@extends('layouts.index')
@section('content-index')
<div class="page-header">
   <h1>
      {{$menu}}
      <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      {{$menu}}
      </small>
   </h1>
</div>
<div class="row">
   <div class="col-xs-12 widget-container-col" id="widget-container-col-12">
      <div class="widget-box transparent" id="widget-box-12">
         <div class="widget-header">
            <h4 class="widget-title lighter">Detail {{$menu}}</h4>
            <div class="widget-toolbar no-border">
               <a href="#" data-action="settings">
               <i class="ace-icon fa fa-cog"></i>
               </a>
               <a href="#" id="reload" data-action="reload">
               <i class="ace-icon fa fa-refresh"></i>
               </a>
               <a href="#" data-action="collapse">
               <i class="ace-icon fa fa-chevron-up"></i>
               </a>
               <a href="#" data-action="close">
               <i class="ace-icon fa fa-times"></i>
               </a>
            </div>
       </div>
       <div id="myDiv"  action="">
            @include('customers._showdetail')
       </div>
      </div>
   </div>
   <div class="col-xs-12">
      @include('customers._session')
      <div id="formupdate"></div>
   </div>
</div>
@endsection


@push('js')
<script type="text/javascript">
jQuery(function($){
    $('#reload').click(function(e){
        e.preventDefault();
        var form = $(this);
        var post_url = form.attr('action');
        var post_data = form.serialize();
        $.ajax({
            url: "{{ route('customer-reload', ['id' => $customer->id]) }}",
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            format: "json",
            data: post_data,
            success: function(data){
               $("#myDiv").load(location.href+" #myDiv>*",true);
               // $("#myDiv").load(location.href+" #myDiv","");
            }
        })
    })
});
</script>
@endpush

