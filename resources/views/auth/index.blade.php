<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" /> {{Html::style('ace-master/assets/css/bootstrap.min.css')}} {{Html::style('ace-master/assets/font-awesome/4.5.0/css/font-awesome.min.css')}} {{Html::style('ace-master/assets/css/fonts.googleapis.com.css')}} {{Html::style('ace-master/assets/css/jquery-ui.min.css')}}
    {{Html::style('ace-master/assets/css/bootstrap-datepicker3.min.css')}} {{Html::style('ace-master/assets/css/ui.jqgrid.min.css')}} {{Html::style('ace-master/assets/css/ace.min.css')}} {{Html::style('ace-master/assets/css/ace-skins.min.css')}} {{Html::style('ace-master/assets/css/ace-rtl.min.css')}}
    {{Html::style('ace-master/assets/css/editable.css')}} {{Html::script('ace-master/assets/js/ace-extra.min.js')}} {{Html::style('ace-master/assets/css/select2.min.css')}}
    <title>Apps Pos</title>
</head>

<body class="login-layout light-login">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="login-container">
                        <div class="space-20"></div>
                        <div class="center">
                            <h1>
                                <i class="ace-icon fa fa-leaf green"></i>
                                <span class="red">App</span>
                                <span class="grey" id="id-text2">POS</span>
                            </h1>
                            <!-- <h4 class="blue" id="id-company-text">&copy; Company Name</h4> -->
                        </div>
                        <div class="space-10"></div>
                        <!-- /.position-relative -->
                        @yield('content-login')
                        <div class="navbar-fixed-top align-right">
                            <br /> &nbsp;
                            <a id="btn-login-light" href="#">Light</a> &nbsp; &nbsp; &nbsp;
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.main-content -->
    </div>
    <!-- /.main-container -->

    {{Html::script('ace-master/assets/js/jquery-2.1.4.min.js')}} {{Html::script('ace-master/assets/js/bootstrap.min.js')}} {{-- {{Html::script('ace-master/assets/js/buttons.flash.min.js')}} {{Html::script('ace-master/assets/js/buttons.html5.min.js')}} {{Html::script('ace-master/assets/js/buttons.print.min.js')}}
    --}} {{Html::script('ace-master/assets/js/jquery-ui.custom.min.js')}} {{Html::script('ace-master/assets/js/jquery.ui.touch-punch.min.js')}} {{Html::script('ace-master/assets/js/jquery.easypiechart.min.js')}} {{Html::script('ace-master/assets/js/jquery.sparkline.index.min.js')}}
    {{Html::script('ace-master/assets/js/jquery.flot.min.js')}} {{Html::script('ace-master/assets/js/jquery.flot.pie.min.js')}} {{Html::script('ace-master/assets/js/jquery.flot.resize.min.js')}} {{Html::script('ace-master/assets/js/bootstrap-datepicker.min.js')}}
    {{Html::script('ace-master/assets/js/jquery.jqGrid.min.js')}} {{Html::script('ace-master/assets/js/grid.locale-en.js')}} {{Html::script('ace-master/assets/js/jquery.dataTables.min.js')}} {{Html::script('ace-master/assets/js/jquery.dataTables.bootstrap.min.js')}}
    {{Html::script('ace-master/assets/js/dataTables.buttons.min.js')}} {{Html::script('ace-master/assets/js/ace-elements.min.js')}} {{Html::script('ace-master/assets/js/ace.min.js')}} {{Html::script('ace-master/assets/js/moment.js')}} {{Html::script('ace-master/assets/js/editable.js')}}
    {{Html::script('ace-master/assets/js/select2.min.js')}} {{-- @puush('js') --}} @stack('js')
    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($) {
            $(document).on('click', '.toolbar a[data-target]', function(e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible'); //hide others
                $(target).addClass('visible'); //show target
            });
        });

        //you don't need this, just used for changing background
        jQuery(function($) {
            $('#btn-login-light').on('click', function(e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
        });
    </script>
    @stack('js')
</body>

</html>