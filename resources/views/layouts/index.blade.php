<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" /> {{Html::style('ace-master/assets/css/bootstrap.min.css')}} {{Html::style('ace-master/assets/font-awesome/4.5.0/css/font-awesome.min.css')}} {{Html::style('ace-master/assets/css/fonts.googleapis.com.css')}} {{Html::style('ace-master/assets/css/jquery-ui.min.css')}}
    {{Html::style('ace-master/assets/css/bootstrap-datepicker3.min.css')}} {{Html::style('ace-master/assets/css/ui.jqgrid.min.css')}} {{Html::style('ace-master/assets/css/ace.min.css')}} {{Html::style('ace-master/assets/css/ace-skins.min.css')}} {{Html::style('ace-master/assets/css/ace-rtl.min.css')}}
    {{Html::style('ace-master/assets/css/editable.css')}} {{Html::script('ace-master/assets/js/ace-extra.min.js')}} 
    {{Html::style('ace-master/assets/css/select2.min.css')}}
    {{Html::style('ace-master/assets/css/dropzone.min.css')}} {{Html::style('ace-master/assets/css/pagination.css')}} 
    {{Html::style('ace-master/assets/css/custome.css')}}
    {{Html::style('ace-master/assets/css/boostrap-select2.css')}}
    
    <title>Apps Pos</title>
</head>

<body class="no-skin">
    @include('layouts.navbar')
    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try {
                ace.settings.loadState("main-container");
            } catch (e) {}
        </script>
        @include('layouts.menu')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="ace-settings-container" id="ace-settings-container"></div>
                    <!-- PAGE CONTENT BEGINS -->
                    @yield('content-index')
                    <!-- PAGE CONTENT ENDS -->
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="footer-inner">
                <div class="footer-content">
                    <span class="bigger-120">
                            <span class="blue bolder">Ace</span>
                    </span>
                    &nbsp; &nbsp; {{--
                    <span class="action-buttons">
                            <a href="#">
                                <i
                                    class="ace-icon fa fa-twitter-square light-blue bigger-150"
                                ></i>
                            </a>
                            <a href="#">
                                <i
                                    class="ace-icon fa fa-facebook-square text-primary bigger-150"
                                ></i>
                            </a>
                            <a href="#">
                                <i
                                    class="ace-icon fa fa-rss-square orange bigger-150"
                                ></i>
                            </a>
                        </span> --}}
                </div>
            </div>
        </div>
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
        </a>
    </div>

    {{Html::script('ace-master/assets/js/jquery-2.1.4.min.js')}} {{Html::script('ace-master/assets/js/bootstrap.min.js')}} {{-- {{Html::script('ace-master/assets/js/buttons.flash.min.js')}} {{Html::script('ace-master/assets/js/buttons.html5.min.js')}} {{Html::script('ace-master/assets/js/buttons.print.min.js')}}
    --}} {{Html::script('ace-master/assets/js/jquery-ui.custom.min.js')}} {{Html::script('ace-master/assets/js/jquery.ui.touch-punch.min.js')}} {{Html::script('ace-master/assets/js/jquery.easypiechart.min.js')}} {{Html::script('ace-master/assets/js/jquery.sparkline.index.min.js')}}
    {{Html::script('ace-master/assets/js/jquery.flot.min.js')}} {{Html::script('ace-master/assets/js/jquery.flot.pie.min.js')}} {{Html::script('ace-master/assets/js/jquery.flot.resize.min.js')}} {{Html::script('ace-master/assets/js/bootstrap-datepicker.min.js')}}
    {{Html::script('ace-master/assets/js/jquery.jqGrid.min.js')}} {{Html::script('ace-master/assets/js/grid.locale-en.js')}} {{Html::script('ace-master/assets/js/jquery.dataTables.min.js')}} {{Html::script('ace-master/assets/js/jquery.dataTables.bootstrap.min.js')}}
    {{Html::script('ace-master/assets/js/dataTables.buttons.min.js')}} {{Html::script('ace-master/assets/js/ace-elements.min.js')}} {{Html::script('ace-master/assets/js/ace.min.js')}} {{Html::script('ace-master/assets/js/moment.js')}} {{Html::script('ace-master/assets/js/editable.js')}}
   
    {{Html::script('ace-master/assets/js/dropzone.min.js')}}
    {{Html::script('ace-master/assets/js/select2.min.js')}}
    {{HTML::script('ace-master/assets/js/autoNumeric/autoNumeric.js')}}
    {{HTML::script('ace-master/assets/js/pagination.min.js')}} {{HTML::script('ace-master/assets/js/simplepagination.js')}} 
    {{HTML::script('ace-master/assets/js/nested_form.js')}} 
    {{HTML::script('ace-master/assets/js/JsBarcode.all.min.js')}}

    <script type="text/javascript">
        jQuery(function($) {
            var $sidebar = $(".sidebar").eq(0);
            if (!$sidebar.hasClass("h-sidebar")) return;

            $(document)
                .on("settings.ace.top_menu", function(
                    ev,
                    event_name,
                    fixed
                ) {
                    if (event_name !== "sidebar_fixed") return;

                    var sidebar = $sidebar.get(0);
                    var $window = $(window);

                    //return if sidebar is not fixed or in mobile view mode
                    var sidebar_vars = $sidebar.ace_sidebar("vars");
                    if (!fixed ||
                        sidebar_vars["mobile_view"] ||
                        sidebar_vars["collapsible"]
                    ) {
                        $sidebar.removeClass("lower-highlight");
                        //restore original, default marginTop
                        sidebar.style.marginTop = "";

                        $window.off("scroll.ace.top_menu");
                        return;
                    }

                    var done = false;
                    $window
                        .on("scroll.ace.top_menu", function(e) {
                            var scroll = $window.scrollTop();
                            scroll = parseInt(scroll / 4); //move the menu up 1px for every 4px of document scrolling
                            if (scroll > 17) scroll = 17;

                            if (scroll > 16) {
                                if (!done) {
                                    $sidebar.addClass("lower-highlight");
                                    done = true;
                                }
                            } else {
                                if (done) {
                                    $sidebar.removeClass("lower-highlight");
                                    done = false;
                                }
                            }

                            sidebar.style["marginTop"] = 17 - scroll + "px";
                        })
                        .triggerHandler("scroll.ace.top_menu");
                })
                .triggerHandler("settings.ace.top_menu", [
                    "sidebar_fixed",
                    $sidebar.hasClass("sidebar-fixed"),
                ]);

            $(window).on("resize.ace.top_menu", function() {
                $(document).triggerHandler("settings.ace.top_menu", [
                    "sidebar_fixed",
                    $sidebar.hasClass("sidebar-fixed"),
                ]);
            });
        });
    </script>
    {{-- @puush('js') --}} @stack('js')
</body>

</html>