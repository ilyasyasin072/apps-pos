<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state">
    <script type="text/javascript">
        try {
            ace.settings.loadState('sidebar')
        } catch (e) {}
    </script>
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div>
    <ul class="nav nav-list">
        @foreach($sidebar_menus as $sidebar)
        <?php $has_submenu = ( count($sidebar['subs']) > 0);
        ?>
        <li class="hover">
            <a href={{$sidebar[ 'url']}} class="{{ $has_submenu ? 'dropdown-toggle' : '' }}">
                <i class="menu-icon {{ $sidebar['icon'] }}"></i>
                <span {{ isset($sidebar['root']) && $sidebar['root'] ? 'menu-text' : '' }}>
                    {{$sidebar['text']}}
                </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
           
            <ul class="submenu">
                @foreach($sidebar['subs'] as $sub)

                <li class="hover">
                    <a href="{{ $sub['url'] }}">
                        <i class="menu-icon fa fa-caret-right"></i> {{ $sub['text'] }}
                    </a>
                    <b class="arrow"></b>
                </li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>
</div>