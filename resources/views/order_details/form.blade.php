<div class="row">
    <div class="">
        <!-- PAGE CONTENT BEGINS -->
        <div class="col-xs-12 col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <?php $menus = 'Form Order Details'; ?>
                    <h4 class="widget-title">{{$menus}}</h4>

                    <span class="widget-toolbar">
                        <a href="#" data-action="settings">
                            <i class="ace-icon fa fa-cog"></i>
                        </a>

                        <a href="#" id="reload" data-action="reload">
                            <i class="ace-icon fa fa-refresh"></i>
                        </a>

                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>

                        <a href="#" data-action="close">
                            <i class="ace-icon fa fa-times"></i>
                        </a>
                    </span>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        @if(!isset($order_details))
                        <form class="form-horizontal" role="form" action="{{ route('order.detail-store') }}" id="ajax">
                            @else
                            <form class="form-horizontal" role="form" action="">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right"
                                        for="form-field-2">order_details ID
                                    </label>
                                    <div class="col-sm-4">
                                        <label class="control-label" for="form-field-2"></label>
                                        <span class="help-inline col-xs-12 col-sm-7">
                                            <span class="middle">
                                                <h5>{{$order_details->id}}</h5>
                                            </span>
                                        </span>
                                    </div>
                                </div>

                                @endif
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right"
                                        for="form-field-2">Email</label>
                                    <div class="col-sm-4">
                                        <select class="" name="product_id" id="products">
                                            <option>--Select Category--</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right"
                                        for="form-field-2">Email</label>
                                    <div class="col-sm-4">
                                        <select class="" name="order_id" id="order">
                                            <option>--Select Category--</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Qty
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="qty" class="col-xs-10 col-sm-3"
                                            id="form-input-readonly"
                                            value="{{ !isset($order_details->qty) ? '' : $order_details->qty }} " />
                                        <span class="help-inline col-xs-12 col-sm-7">Pcs
                                        </span>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Price
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="price" class="col-xs-10 col-sm-5"
                                            id="price-form" data-a-sign="Rp. " data-a-dec="," data-a-sep="."
                                            value="{{ !isset($order_details->price) ? '' : $order_details->price }} " />
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        </span>
                                    </div>
                                </div>


                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-5 col-md-9">
                                        <button class="btn btn-info" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Submit
                                        </button> &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="alert"></div>
<script type="text/javascript">
    $(document).ready(function() {
        ProductSelect();
            $("#products").select2();
        OrderSelect();
            $("#order").select2();
            $('#alert').hide();

            $('#price-form').autoNumeric('init');
    });

    function ProductSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('product-detail-api') }}",
            success: function(result) {
                var data = result.product;
                data.forEach(products => {
                    $("#products").append("<option value='" + products.id + "'>" + ' Product : ' + products.name_product + "</option>");
                });
            },
            async: false
        })
    }

    function OrderSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('order-detail-api') }}",
            success: function(result) {
                var data = result.order;
                data.forEach(order => {
                    $("#order").append("<option value='" + order.id + "'>" + ' Product : ' + order.invoice + "</option>");
                });
            },
            async: false
        })
    }

    function customers() {
       jQuery(function($) {
           var t = $("#orderdetail-table").DataTable({
               processing: true,
               serverSide: true,
               bAutoWidth: false,
               // dom: 'rti',
               ajax: "{{ route('orderdetail-api') }}",
               search: {
                   caseInsensitive: true,
               },
               columns: [{
                   data: "id",
                   name: "id",
               }, {
                   data: "product.name",
                   name: "product.name",
               }, {
                   data: "order.invoice",
                   name: "order.invoice",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "order.customer.name",
                   name: "order.customer.name",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "price",
                   name: "price",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "created_at",
                   name: "created_at",
                   orderable: false,
                   searchable: false,
                   render: function(d) {
                       return moment(d).format("dddd, DD-MM-YYYY");
                   },
               }, {
                   data: "action",
                   name: "action",
                   orderable: false,
                   searchable: false,
               }, ],
               order: [
                   [0, "desc"]
               ],
               pageLength: 15,
               select: {
                   style: "multi",
               },
           });
       });
   }


    jQuery(function($) {
    $('#ajax').submit(function(event){
        event.preventDefault(); 
            $.ajax({
                type:"POST",
                url:"{{ route('order.detail-store') }}",
                dataType:"json",
                data:$('#ajax').serialize(),
                success: function(data){
                    var alert = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Berhasil Di simpan\
                                            </p>\
                                        </div>\
                                    </div>';
                    $('#alert').html(alert).show();
                    $("#orderdetail-table").DataTable().ajax.reload();
                },
                error: function(data){
                    alert("Error")
                }
            });  
        })
    });
</script>