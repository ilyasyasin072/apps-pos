

@extends('layouts.index')
@section('content-index')
<?php $menu = 'Customers' ?>
<div class="page-header">
   <h1>
      {{$menu}}
      <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      {{$menu}}
      </small>
   </h1>
</div>
@include('categories._session') 
<div class="row">
   <div class="col-xs-12 col-sm-12">
      <div class="pull-right" style="margin-bottom: 10px;">
         <button class="btn btn-sm btn-primary" onclick="showCreate();"><i class="ace-icon fa fa-plus bigger-120"></i>&nbsp;Create Customer</button>
      </div>
      <div id="form"></div>
   </div>
   <div class="col-xs-12 col-sm-12"></div>
   <div class="col-xs-12 col-sm-12">
      <div class="clearfix">
         <div class="pull-right tableTools-container"></div>
      </div>
      <div class="table-header">Result {{ $menu }}</div>
      <div>
         <table id="orderdetail-table" class="table table-striped table-bordered table-hover" >
            <thead>
               <tr>
                  <th>Nomor Customers</th>
                  <th>Name Customers</th>
                  <th>Email Customers</th>
                  <th>Address Customers</th>
                  <th>Phone Customers</th>
                  <th>Date Customers</th>
                  <th></th>
               </tr>
            </thead>
         </table>
      </div>
   </div>
</div>
@endsection @push('js')
<script type="text/javascript">
   jQuery(document).ready(function() {
       customers();
			//  JsonProduct();
   });
   
   function customers() {
       jQuery(function($) {
           var t = $("#orderdetail-table").DataTable({
               processing: true,
               serverSide: true,
               bAutoWidth: false,
               // dom: 'rti',
               ajax: "{{ route('orderdetail-api') }}",
               search: {
                   caseInsensitive: true,
               },
               columns: [{
                   data: "id",
                   name: "id",
               }, {
                   data: "product.name",
                   name: "product.name",
               }, {
                   data: "order.invoice",
                   name: "order.invoice",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "order.customer.name",
                   name: "order.customer.name",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "price",
                   name: "price",
                   orderable: false,
                   searchable: false,
               }, {
                   data: "created_at",
                   name: "created_at",
                   orderable: false,
                   searchable: false,
                   render: function(d) {
                       return moment(d).format("dddd, DD-MM-YYYY");
                   },
               }, {
                   data: "action",
                   name: "action",
                   orderable: false,
                   searchable: false,
               }, ],
               order: [
                   [0, "desc"]
               ],
               pageLength: 15,
               select: {
                   style: "multi",
               },
           });
       });
   }

   function autoRefreshData() {
       $("#customers-table").DataTable().ajax.reload();
   }
   
   setInterval("autoRefreshData()", 10000);
   
   
   function showCreate() {
       event.preventDefault();
       const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
   
       $.ajax({
           url: "/web/orderdetail/create",
           type: "GET",
           data: {
               CSRF_TOKEN,
           },
           success: function(data) {
               // console.log(data);
               $("#form").html(data);
           },
       });
   }
</script>
@endpush

