<div class="col-xs-12 col-sm-4 col-md-4">
    <div class="widget-box transparen">
        <!-- <div class="widget-header">
        </div> -->
        <div class="widget-body">
            <div class="col-xs-12 col-sm-12">
                <!-- percentage -->
                @include('dashboard.top.percentage')
            </div>

        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-8 col-md-8">
    <div class="widget-box">
        <!-- <div class="widget-header">
        </div> -->
        <div class="widget-body">
            <!-- Chart  -->
            @include('dashboard.top.chart')
        </div>
    </div>
</div>