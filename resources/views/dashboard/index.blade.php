@extends('layouts.index') @section('content-index')
<div class="content">
    {{-- <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <!-- dashboard top -->
                    @include('dashboard.top.top')
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <!-- dashboard center -->
                    @include('dashboard.center.center')
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <!-- dashboard bottom -->
                    @include('dashboard.bottom.bottom')
                </div>
            </div>
        </div>
    </div> --}}
    <style>
        .padding-dashboard {
          background-color: #68ad00;
          /* padding: 10px; */
          color: #ffff;
          border-radius: 10px;
        }
  
        .padding-dashboard-2 {
          background-color: #fd8213;
          /* padding: 10px; */
          color: #ffff;
          border-radius: 10px;
        }
  
        .padding-dashboard-3 {
          background-color: #337bb7;
          /* padding: 10px; */
          color: #ffff;
          border-radius: 10px;
        }
  
        .statistik-padding {
          padding: 10px;
        }
  
        .total-dashboard {
          font-size: 30px;
          text-align: center;
        }
  
        .desc-dashboard {
          font-size: 20px;
          text-align: center;
          font-weight: bold;
          text-transform: uppercase;
        }
  
        .child-dashboard {
          font-size: 24px;
          text-align: center;
        }
      </style>
      <div class="col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <div class="card padding-dashboard">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <h3 class="total-dashboard">53243</h3>
                        <p class="desc-dashboard">
                          <h5 class="child-dashboard">Quotation</h5>
                        </p>
                      </div>
                      <div class="col-sm-6">
                        <div style="text-align: center; margin-top: 10px;">
                          <i class="fa fa-list-alt" style="font-size: 6em;"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 col-xs-12">
                <div class="card padding-dashboard-2">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <h3 class="total-dashboard">5321</h3>
                        <p class="desc-dashboard">
                          <h5 class="child-dashboard">Purchase Inquiri</h5>
                        </p>
                      </div>
                      <div class="col-sm-6">
                        <div style="text-align: center; margin-top: 10px;">
                          <i class="fa fa-tasks" style="font-size: 6em;"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 col-xs-12">
                <div class="card padding-dashboard-3">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <h3 class="total-dashboard">5332</h3>
                        <p class="desc-dashboard">
                          <h5 class="child-dashboard">example</h5>
                        </p>
                      </div>
                      <div class="col-sm-6">
                        <div style="text-align: center; margin-top: 10px;">
                          <i class=" fa fa-bell" style="font-size: 6em;"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="statistik-padding">
                      <div class="statistik-padding">
                        <h4>Statistics <strong>Quotation</strong> Last 3 Data</h4>
                        <table class="table table-bordered table-stripped" width="100%" id="example">
                          <thead>
                            <tr>
                              <th rowspan="2" style="text-align: middle;">example</th>
                              <th colspan="2" style="text-align: center;">example</th>
                              <th colspan="2" style="text-align: center;">example</th>
                            </tr>
                            <tr>
                              <th>example</th>
                              <th>example</th>
                              <th>example</th>
                              <th>action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><a href="#" class="blue">5007</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                            <tr>
                              <td><a href="#" class="blue">5887</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                            <tr>
                              <td><a href="#" class="blue">5980</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="statistik-padding">
                      <div class="statistik-padding">
                        <h4>Statistics Purchase Last 3 Data</h4>
                        <table class="table table-bordered table-stripped" width="100%" id="example1">
                          <thead>
                            <tr>
                              <th rowspan="2" style="text-align: middle;">example</th>
                              <th colspan="2" style="text-align: center;">example</th>
                              <th colspan="2" style="text-align: center;">example</th>
                            </tr>
                            <tr>
                              <th>example</th>
                              <th>example</th>
                              <th>example</th>
                              <th>action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><a href="#" class="blue">5674</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                            <tr>
                              <td><a href="#" class="blue">5512</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                            <tr>
                              <td><a href="#" class="blue">5341</a></td>
                              <td>example</td>
                              <td>example</td>
                              <td>example</td>
                              <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="">
                      <div class="">
                        <h4>Statistics Yarn List Price</h4>
                        <div class="card-header-action">
                          <select class="form-control select2" name="chart" id="chart"></select>
                        </div>
                        <hr>
                        <div class="infobox-chart">
                                <div id="piechart-placeholder"></div>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="row">
                  <div class="col-sm-12">
  
                      <div class="statistik-padding">
                          <div class="statistik-padding">
                            <h4>Statistics Purchase Last 3 Data</h4>
                            <table class="table table-bordered table-stripped" width="100%" id="example3">
                              <thead>
                                <tr>
                                  <th rowspan="2" style="text-align: middle;">example</th>
                                  <th colspan="2" style="text-align: center;">example</th>
                                  <th colspan="2" style="text-align: center;">example</th>
                                </tr>
                                <tr>
                                  <th>example</th>
                                  <th>example</th>
                                  <th>example</th>
                                  <th>action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td><a href="#" class="blue">5674</a></td>
                                  <td>example</td>
                                  <td>example</td>
                                  <td>example</td>
                                  <td><a href="#" class="blue"><i class="fa fa-tag"></i></a></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>

@endsection @push('js')
<script type="text/javascript">
    jQuery(function($) {
        $(document).ready(function() {
            console.log('ready dashboard');
        })

        $('#example').DataTable({
      orderClasses: false,
    'stripeClasses':['stripe1','stripe2']
    });
    $('#example1').DataTable({
      orderClasses: false,
    'stripeClasses':['stripe1','stripe2']
    });
    $('#example3').DataTable({
      orderClasses: false,
    'stripeClasses':['stripe1','stripe2']
    });
    })
    
    var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "social networks",  data: 38.7, color: "#68BC31"},
				{ label: "search engines",  data: 24.5, color: "#2091CF"},
				{ label: "ad campaigns",  data: 8.2, color: "#AF4E96"},
				{ label: "direct traffic",  data: 18.6, color: "#DA5430"},
				{ label: "other",  data: 10, color: "#FEE074"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
</script>
@endpush