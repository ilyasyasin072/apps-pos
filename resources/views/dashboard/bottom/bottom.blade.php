<div class="col-xs-12 col-sm-12">
    <div class="widget-box">
        <div class="card">
            <div class="card-body">
                <div class="widget-header">
                </div>
                <div class="widget-body">
                    <!-- table product/order/customer can filter only -->
                    <div class="col-xs-12 col-sm-12" style="margin-top: 10px;">
                        @include('dashboard.bottom.datatable')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>