@extends('layouts.index') @section('content-index')

<div class="page-header">
    <h1>
        {{ $menus }}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{ $menus }}
        </small>
    </h1>
</div>
<!-- /.page-header -->
@include('categories._session')

<div class="row">
    <div class="col-sm-12">
        <div class="pull-right">
            <button class="btn btn-sm btn-primary" onclick="routeIndex();">
                <i class="ace-icon fa fa-plus bigger-120"></i>&nbsp; Create Category
            </button>
        </div>
    </div>
    <hr />
    <div class="col-sm-12" id="form"></div>
    <div class="col-sm-12"></div>
    <div class="col-sm-12">
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">Result {{ $menus }}</div>
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No Category</th>
                        <th>Category Name</th>
                        <th>Category Description</th>
                        <th>Create Date</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    jQuery(document).ready(function() {
        categories();
    });

    function categories() {
        jQuery(function($) {
            var t = $("#dynamic-table").DataTable({
                processing: true,
                serverSide: true,
                bAutoWidth: false,
                // dom: 'rti',
                ajax: "{{ route('category-api') }}",
                search: {
                    caseInsensitive: true,
                },
                columns: [{
                    data: "id",
                    name: "id",
                }, {
                    data: "name",
                    name: "name",
                }, {
                    data: "description",
                    name: "description",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "created_at",
                    name: "created_at",
                    orderable: false,
                    searchable: false,
                    render: function(d) {
                        return moment(d).format("dddd, DD-MM-YYYY");
                    },
                }, {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                }, ],
                order: [
                    [0, "desc"]
                ],
                pageLength: 15,
                select: {
                    style: "multi",
                },
                "columnDefs": [
                    {  "title": "<span style='color:default'>Action</span>", className: "text-center", "targets": [ 4 ]},
        ],
            });
        });
    }

    // function autoRefreshData() {
    //     $("#dynamic-table").DataTable().ajax.reload();
    // }

    // setInterval("autoRefreshData()", 10000);

    function routeIndex() {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/category/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }

    function routeUpdate(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/category/edit" + "/" + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }

    function routeShow(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/category/show" + "/" + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#show").html(data);
            },
        });
    }
</script>
@endpush