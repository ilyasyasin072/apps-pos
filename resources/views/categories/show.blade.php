<div class="card">
    <div class="card-body">
            <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat">
                                <h4 class="widget-title smaller">
                                    <i class="ace-icon fa fa-quote-left smaller-80"></i>
                                    {{$category->name}}
                                </h4>
                                <a href="#" data-action="close">
                                    <i class="ace-icon fa fa-times"></i>
                                </a>
                            </div>
                
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <blockquote class="pull-right">
                                                <p>{{$category->name}}</p>
                
                                                <small>
                                                    {{$category->description}}
                                                    <cite title="Source Title">Source Title</cite>
                                                </small>
                                            </blockquote>
                                        </div>
                                    </div>
                
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <blockquote>
                                                <p class="lighter line-height-125">
                                                    >{{$category->name}}
                                                </p>
                
                                                <small>
                                                    {{$category->description}}
                                                    <cite title="Source Title">Source Title</cite>
                                                </small>
                                            </blockquote>
                                        </div>
                                    </div>
                
                                    <hr />
                                    <address>
                                        <strong>Twitter, Inc.</strong>
                
                                        <br />
                                        795 Folsom Ave, Suite 600
                                        <br />
                                        San Francisco, CA 94107
                                        <br />
                                        <abbr title="Phone">P:</abbr>
                                        (123) 456-7890
                                    </address>
                
                                    <address>
                                        <strong>Full Name</strong>
                
                                        <br />
                                        <a href="mailto:#">first.last@example.com</a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>