<span class="blue">
    <a class="blue" href="" onclick="routeShow({{ $categoryd->id }});">
        <i class="ace-icon fa fa-eye bigger-120"></i>
    </a>
</span>
<span class="green">
    <a class="green" href="" onclick="routeUpdate('{{ $categoryd->id }}');">
        <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
    </a>

</span>
<span class="red">
    <a class="red" href="{{ url('/web/category/delete', ['id' => $categoryd->id]) }}">
        <i class="ace-icon fa fa-trash-o bigger-120"></i>
    </a>
</span>