<div class="widget-box tab-content2" id="detail-form" style="display:none">
    <div class="widget-header widget-header-flat">
        <h4 class="widget-title smaller">Definition List</h4>

        <div class="widget-toolbar">
            <label>
                <small class="green">
                    <b>Horizontal</b>
                </small>

                <input
                    id="id-check-horizontal"
                    type="checkbox"
                    class="ace ace-switch ace-switch-6"
                />
                <span class="lbl middle"></span>
            </label>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
            <code class="pull-right" id="dt-list-code">&lt;dl&gt;</code>

            <dl id="dt-list-1" class="form-detail"></dl>
        </div>
    </div>
    <div class="col-sm-12 col-xs-12" style="margin-top: 20px">
        <div class="row">
            <div class="col-sm-12">
                <div class="clearfix">
                    <div class="pull-right tableTools-container"></div>
                </div>
                <div class="pull-right">
                    <button class="btn btn-sm btn-primary" id="save">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        Simpan Ke Keranjang
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="price"></div>
</div>
@push('js')
<script style="text/javascript">
    function add(id) {
        var newCount = parseInt($(id).text()) + 1;
        let data = $(id).text(newCount);
        $("#input").val(newCount);
    }

    function substract(id) {
        var newCount = parseInt($(id).text()) - 1;
        $(id).text(newCount);
        $("#input").val(newCount);
    }

    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split("").reverse().join("");
        var rev2 = "";
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== rev.length - 1) {
                rev2 += ".";
            }
        }
        return "Rp. " + rev2.split("").reverse().join("") + ",00";
    }

    $(document).ready(function() {
        var btn_save = "{{  $order->total }}";
        var inv = '<h3>' + toRp(btn_save) + '</h3>';
        if (btn_save != 0) {
            $('#save').attr("style", "display:none");
            $('#table-form-all').attr("style", "display:none");
            $('#filter-product').attr("disabled", "disabled");
            $('#total-harga').html(inv).show();
            // $('#save').attr("readonly", "true");
        }
        var html =
            '<div class="col-sm-12">\
                    <div class="col-sm-6">\
                        <dd>\
                            <img src="/uploads/product/product-default.jpg" alt="" width="100%" height="200px" />\
                        </dd>\
                    </div>\
                    <div class="col-sm-6">\
                        <table class="table">\
                            <thead>\
                                <tr>\
                                    <strong>\
                                        <td>Dompet Pria Bracini Dexter Hitam</td>\
                                    </strong>\
                                </tr>\
                                <tr>\
                                    <td>Terjual 543 Produk51271x Dilihat</td>\
                                </tr>\
                                <tr>\
                                    <td>Pasti Ready</td>\
                                </tr>\
                                <tr>\
                                    <td>Jumlah : \
                                </tr>\
                                <tr>\
                                    <table class="table table-bordered">\
                                        <thead>\
                                            <tr class="desc">\
                                                <td>Berat Barang</td>\
                                                <td>Kondisi</td>\
                                            </tr>\
                                        </thead>\
                                        <tbody>\
                                            <tr class="desc">\
                                                <td>26 Gram</td>\
                                                <td>Baru</td>\
                                            </tr>\
                                        </tbody>\
                                    </table>\
                                </tr>\
                            </thead>\
                        </table>\
                    </div>\
                </div>';
        $(".form-detail").append(html);
    });

    window.prettyPrint && prettyPrint();
    $("#id-check-horizontal")
        .removeAttr("checked")
        .on("click", function() {
            $("#dt-list-1")
                .toggleClass("dl-horizontal")
                .prev()
                .html(this.checked ? "Horizontal" : "list");
        });

    function getProductDetail(product = "") {
        $.ajax({
            type: "POST",
            url: "{{ route('product-detail-id') }}" + "?key=" + product,
            success: function(result) {
                var data = result.product;
                // console.log(data);
                $(".form-detail").empty();
                if (data === undefined) {
                    var html =
                        '<div class="col-sm-12">\
                    <div class="col-sm-6">\
                        <dd>\
                            <img src="/uploads/product/product-default.jpg" alt="" width="100%" height="200px" />\
                        </dd>\
                    </div>\
                    <div class="col-sm-6">\
                        <table class="table">\
                            <thead>\
                                <tr>\
                                    <strong>\
                                        <td>Dompet Pria Bracini Dexter Hitam</td>\
                                    </strong>\
                                </tr>\
                                <tr>\
                                    <td>Terjual 543 Produk51271x Dilihat</td>\
                                </tr>\
                                <tr>\
                                    <td>Pasti Ready</td>\
                                </tr>\
                                <tr>\
                                    <td>Jumlah</td>\
                                </tr>\
                                <tr>\
                                    <table class="table table-bordered">\
                                        <thead>\
                                            <tr class="desc">\
                                                <td>Berat Barang</td>\
                                                <td>Kondisi</td>\
                                            </tr>\
                                        </thead>\
                                        <tbody>\
                                            <tr class="desc">\
                                                <td>26 Gram</td>\
                                                <td>Baru</td>\
                                            </tr>\
                                        </tbody>\
                                    </table>\
                                </tr>\
                            </thead>\
                        </table>\
                    </div>\
                </div>';
                    $(".form-detail").append(html);
                    $('#detail-form').attr("style", "display:none");
                } else {
                    data.forEach((product) => {
                        // console.log(product);
                        var html =
                            '<div class="col-sm-12">\
                    <div class="col-sm-3">\
                        <dd> <img src="/uploads/product/' +
                            product.photo +
                            '" alt="" width="300px;" height="300px"></dd>\
                        </dd>\
                    </div>\
                    <div class="col-sm-9">\
                        <table class="table">\
                            <thead>\
                                <tr>\
                                    <strong>\
                                        <td>' +
                            product.category.name +
                            "</td>\
                                    </strong>\
                                </tr>\
                                <tr>\
                                    <td>Terjual 543 Produk51271x Dilihat " +
                            product.description +
                            "</td>\
                                </tr>\
                                <tr>\
                                    <td>" +
                            toRp(product.price) +
                            '</td>\
                                </tr>\
                                <tr>\
                                    <td>Jumlah : \
                                       <table class="table" style="width:10px;">\
                                        <td width="5%">\
                                        <button id="minus" class="btn btn-xs btn-warning counter-btn" onclick=substract(".output")>\
                                            <i class="ace-icon fa fa-minus bigger-120"></i>\
                                        </button>\
                                        </td>\
                                        <td class="desc" width="5%">\
                                        <input type="text" id="input" class="form-control" value="1" style="width: 50px;"><div class="output" hidden>1</div></p>\
                                        </td>\
                                        <td width="5%">\
                                            <button id="plus" class="btn btn-xs btn-warning counter-btn" onclick=add(".output")>\
                                            <i class="ace-icon fa fa-plus bigger-120"></i>\
                                            </button>\
                                        \</td>\
                                       </table>\
                                            </td>\
                                </tr>\
                                <tr>\
                                    <table class="table table-bordered">\
                                        <thead>\
                                            <tr class="desc">\
                                                <td>Berat Barang</td>\
                                                <td>Kondisi</td>\
                                            </tr>\
                                        </thead>\
                                        <tbody>\
                                            <tr class="desc">\
                                                <td>28 Gram</td>\
                                                <td>Baru</td>\
                                            </tr>\
                                        </tbody>\
                                    </table>\
                                </tr>\
                            </thead>\
                        </table>\
                    </div>\
                </div>';
                        document.getElementById("price").value = product.price;
                        $('#detail-form').removeAttr("style", "display:");
                        $(".form-detail").append(html);
                    });
                }
            },
            async: false,
        });
    }
</script>
@endpush