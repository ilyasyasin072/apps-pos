<?php $menu = 'Category'; ?>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="table-header">Result {{ $menu }}</div>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <select class="select2 from-control" id="filter-users" style="width:100%!important;" multiple="multiple">
        </select>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="scrollit">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nama Category</th>
                        <th>Nama Category</th>
                    </tr>
                </thead>
                <tbody id="form-user"></tbody>
            </table>
        </div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>
        </nav>
    </div>
</div>
@push('js')
<script type="text/javascript">
    $(document).ready(function(){
    UserSelect();
    $('#filter-users').select2();
    })
    function UserSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('user-detail-api') }}",
            success: function(result) {
                var data = result.user;
                data.forEach(user => {
                    $("#filter-users").append("<option value='" + user.id + "'>" + ' User : ' + user.user_name + "</option>");
                });
            },
            async: false
        })
    }

    getUser();

    function getUser(user = '') {
        $.ajax({
            type: "POST",
            url: "{{ route('user-multiple') }}" + '?key=' + user,
            success: function(result) {
                var data = result.user;
                console.log(data);
                $('#form-customer').empty();
                data.forEach(user => {
                    var html = '<tr>\
                    <td>'+user.user_name+'</td>\
                    <td>'+user.email+'</td>\
                    </tr>';
                    $('#form-user').append(html);
                });
                
            },
            async: false
        })

    }

    $('#filter-users').change(function(){
        let userFilter = $(this).val();
        if (userFilter != '') {
            getUser(userFilter);
        } else {
            getUser();
        }
    })



    // $('#demo').pagination({
    //     dataSource: function(done) {
    //     $.ajax({
    //         type: "POST",
    //         url: "{{ route('user-detail-api') }}",
    //         success: function(response) {
    //             var data = response.user;
    //             done(data);
    //         }
    //     });
    //  }
    // })


</script>
@endpush