@extends('layouts.index') @section('content-index')
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php $now = \Carbon\Carbon::now(); ?> {!! Former::date('date')->name('') ->value($now) ->placeholder('PO Pesanan (hanya untuk repeat order yang belum punya SO!)')->label('PO Pesanan') ->addGroupClass('has-warning special-field') !!}
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Former::textarea('Casir') ->rows(5) ->value(Auth::user()->name) ->columns(10)->readonly() ->autofocus(); !!}
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Former::textarea('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty')->value($customer_name)->readonly() ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <!-- <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                            </div>
                                            <div class="col-sm-4" style="margin-top: 25px;">
                                                <input type="text" class="btn btn-primary" value="Barcode">
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-sm-12">
                                        {!! Former::text('job.total_weight')->name('job[total_weight]') ->placeholder('Code')->label('Code') ->addClass('automoney') ->append('kg') ->required()->addGroupClass('has-warning special-field') !!}
                                    </div> -->
                                    <div class="col-sm-12">
                                        <!-- <div class="row">
                                            <div class="panel">
                                                <div class="panel-body">
                                                    {{-- <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" /> --}}
                                                    <img src="data:image/png;base64, {{ DNS1D::getBarcodePNG('4', 'C39+',3,33) }}" alt="barcode" width="100%" /> {{-- {!! DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T',3,33,'green', true); !!} --}}
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-xs-12 id=" form-product-search ">
                                            <div class="tabbable ">
                                                <ul class="nav nav-tabs " id="myTab ">
                                                    <li class="active ">
                                                        <a data-toggle="tab " href="#home ">
                                                            <i class="green ace-icon fa fa-home bigger-120 "></i> Product
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content ">
                                                    <div id="home " class="tab-pane fade in active ">
                                                        @include('orders._each_table_product')
                                                    </div>

                                                    <div id="messages " class="tab-pane fade "></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 ">
                        <div class="row ">
                            <div class="panel panel-default ">
                                <div class="panel-body ">
                                    <div class="row ">
                                      <div class="col-sm-12 ">
                                          <div class="row col-md-6 ">
                                            <div class="center ">
                                                <img src="https://img2.pngdownload.id/20180504/phe/kisspng-professional-computer-icons-avatar-job-5aec571ec854c8.3222584415254382388206.jpg " alt=" " width="50% ">
                                            </div>
                                          </div>
                                          <div class="row col-md-6 " style="font-weight: bold ">
                                             <div class="row ">
                                                <label for=" ">Name</label>
                                                <label for=" "><strong>{{ Auth::user()->name }}</strong></label>
                                             </div>
                                             <div class="row ">
                                                <label for=" ">Position</label>
                                                <label for=" ">kasir</label>
                                             </div>
                                             <div class="row ">
                                                <label for=" ">Position</label>
                                                <label for=" ">{{ Auth::user()->email }}</label>
                                             </div>
                                          </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="panel panel-default ">
                                <div class="panel-body ">
                                    <div class="row ">
                                        <div class="col-sm-12 ">
                                            <div class="pull-left ">
                                                <h3>Invoice</h3>
                                            </div>
                                            <div class="pull-right ">
                                               <div id="inv-show"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 ">
                                            <div class="pull-right ">
                                                <h1>
                                                    <strong><p id="total-harga"></p></strong>
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 " id="form-product-desc ">
                            <div id="home1 " class="tab-pane fade in active ">
                                @include('orders._each_table_product_detail')
                            </div>
            </div>
            <div class="col-sm-12 col-xs-12 ">
                <div class="row ">
                    <!-- <div class="col-sm-12 "> -->
                    <!-- <table class="table table-bordered " id="cart-table ">
                            <thead>
                                <tr>
                                    <th>Barcode</th>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10% ">
                                        <img src="data:image/png;base64, {{ DNS1D::getBarcodePNG( '4', 'C39+',3,33) }} " alt="barcode " />
                                    </td>
                                    <td>Shampo Merek Abal Abal PT Example</td>
                                    <td>4</td>
                                    <td>Rp. {{ number_format(10000, 2, '.', ',')}}</td>
                                    <td width="5% ">
                                        <div class="center ">
                                            <button class="btn btn-xs btn-primary "> <i class="fa fa-edit "></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger "> <i class="fa fa-trash "></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table> -->
                    @include('orders._each_order_detail', ['order' => $order])
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="row ">div>
        </div>
        

        <div id="messages " class="tab-pane fade "></div>

        <div class="clearfix ">
            <div class="pull-right tableTools-container "></div>
        </div>

        <div class="clearfix ">
            <div class="pull-right tableTools-container "></div>
        </div>
        <div class="col-xs-12 col-sm-12 " id="detail-table " style="visibility:hidden; ">
            <div class="tabbable ">
                <ul class="nav nav-tabs " id="myTab ">
                    <li class="active ">
                        <a data-toggle="tab " href="#home2 ">
                            <i class="green ace-icon fa fa-home bigger-120 "></i> Order Detail
                        </a>
                    </li>
                </ul>
                <div class="tab-content tab-content1 ">
                    <div id="home2 " class="tab-pane fade in active ">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection @push('js')
<script type="text/javascript ">
$(document).ready(function() {
    let check = "{{ $order->total }}";
    let statusapprove = "{{ $order->status }}";
    if (check == 0) {
        $("#form-product-search").show();
        $("#form-product-desc").show();

    } else {
        $("#form-product-search").hide();
        $("#form-product-desc").hide();

        $("#btn-check-out").attr("disabled", "disabled");
    }
    if (statusapprove === 1) {
        getOrderDetailFix();
    }
});
$('#cart-table').DataTable();
CustomerSelect();

function CustomerSelect() {
    $.ajax({
        type: "POST",
        url: "{{ route('customer-detail-api') }}",
        success: function(result) {
            var data = result.customer;
            data.forEach(customer => {
                // $("#profiles-thread").select2({ theme: "bootstrap" }); 
                $("#profiles-thread").append(" <option value='" + customer.id + "'>" + '#' + customer.id + '#' + customer.customer_name + "</option>");
            });
        },
        async: false
    })
}
    </script>
 @endpush