@extends('layouts.index') @section('content-index')
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="pull-right">
                <a href="{{ url('web/order/paid/index') }}" class="btn btn-sm btn-info">Back</a>
                <a href="#" class="btn btn-sm btn-primary" id="invoice">Invoice</a>
                <a href="#" class="btn btn-sm btn-warning" id="back" style="display:none">Hide</a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12" id="detail-table" style="visibility:hidden;">
            <div class="tabbable" id="detail-invoice">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active">
                        <a data-toggle="tab" href="#home2">
                            <i class="green ace-icon fa fa-home bigger-120"></i> Order Detail
                        </a>
                    </li>
                </ul>
                <div class="tab-content tab-content-paid">
                    <div id="home2" class="tab-pane fade in active">
                        <div id="status"></div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-sm-12" id="form-all-table" style="visibility:hidden">
                                    <table class="table table-bordered">
                                        <thead>
                                            <th>Kode Product</th>
                                            <th>Name Product</th>
                                            <th>Descrition</th>
                                            <th>Qty</th>
                                            <th>Price Pcs</th>
                                        </thead>
                                        <tbody id="form-order-detail"></tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12" id="form-all-div" style="visibility:hidden">
                                    <div>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <td>Invoice</td>
                                                <td>Customer</td>
                                                <td>total</td>
                                                <td>status</td>
                                            </tbody>
                                            <tbody id="form-all"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="tabbable" id="invoice-tabs" style="display:none">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active">
                        <a data-toggle="tab" href="#home2">
                            <i class="green ace-icon fa fa-home bigger-120"></i> Order Detail
                        </a>
                    </li>
                </ul>
                <div class="tab-content tab-content-invoice">
                    <div id="home2" class="tab-pane fade in active">
                        @include('orders._invoice', ['paid' => $paid, 'order_detail' => $order_detail])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    $(document).ready(function() {
        $("#form-total").attr("style", "visibility:");
        $("#form-all-div").attr("style", "visibility:");
        $('#form-all-table').attr("style", "visibility:");
        let status = "{{ $order->return_money }}";

        getOrderDetailFix();
        $("#form-order-detail-fix").attr("style", "visibility:");
        $('#detail-table').attr("style", "visibility:");
        $('#totaldiv').attr("style", "display:none");
    });

    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split("").reverse().join("");
        var rev2 = "";
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== rev.length - 1) {
                rev2 += ".";
            }
        }
        return "Rp. " + rev2.split("").reverse().join("") + ",00";
    }

    $(document).ready(function() {
        getOrderDetail();
        // checkTotal();
        let totalbayar = "{{ $order->total }}";
        let statusBayar = "{{ $order->status }}";
        $('#totalprice').val(toRp(totalbayar));

        if (statusBayar === 0) {
            $('#totaldiv').attr("style", "display:");
        } else {
            $('#detail-table').attr("style", "visibility:");
        }
    });

    function myFunc(total, bayar) {
        return bayar - total;
    }

    $('#bayar').change(function() {
        let total = "{{ $order->total }}";
        let id = "{{ $order->id }}";
        let bayar = $(this).val();
        // console.log(bayar)
        numbers = [total, bayar];
        let bayar1 = numbers.reduce(myFunc);
        var html = '<p>' + toRp(bayar1) + '</p>';
        $('#total_bayar').html(html).show();
        $('.total').attr("style", "display:");
    });

    $('#transaction').click(function() {
        checkTotal();
    })


    function checkTotal() {
        let checktotal = "{{ $order->total }}";
        let id = "{{ $order->id }}";
        if (checktotal == 0) {} else {
            $("#btn-check-out").attr("disabled", "disabled");
            $("#btn-invoice-out").removeAttr("style", true);
        }
        getOrderDetail();

        $.ajax({
            url: "{{ url('web/order/status/bayar/')}}" + '/' + id,
            type: "POST",
            dataType: "JSON",
            data: {
                total_bayar: checktotal
            },
            success: function(result) {
                $('#totaldiv').attr("style", "display:none");
                console.log(result);
            }
        })
    }

    function getOrderDetail() {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        var orderId = "{{ $order->id }}";
        $('#qty-update').editable({
            type: 'text',
            title: 'Enter username',
            url: '/post',
            display: function(value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = $('<div>').html('<a href="#">' + value + '</a>');
                $(this).html(html);
            }
        });
        $.ajax({
            url: "{{ url('/api/v1/orderdetail/datafix/') }}" + "/" + orderId,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                var data = result.order_detail;
                var total = 0;
                $("#form-order-detail1").hide();
                $("#form-order-detail").empty();
                data.forEach((orderDetail) => {
                    total +=
                        parseFloat(orderDetail.price) *
                        parseFloat(orderDetail.qty);
                    if (total !== 0) {
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                        $("#sum").text(toRp(total));
                    } else {
                        $("#sum").text(total);
                    }
                    let totalFixed = "{{ $order->total }}";
                    var deleted = '';
                    var html =
                        "<tr>\
                                    <td>" +
                        orderDetail.code +
                        "</td>\
                                    <td>" +
                        orderDetail.name +
                        "</td>\
                                    <td>" +
                        orderDetail.description +
                        "</td>\
                        <td class='total desc'> <span><a href='#' id='qty-update' data-type='text' data-pk='" + orderDetail.id + "' data-name='qty' data-url='{{ url('api/v1/orderdetail/update/qty/" + orderDetail.id + "')}}'>" + orderDetail.qty + "</a></span></td> \
                        <td class='total'>" +
                        toRp(orderDetail.price) +
                        "</td>" +
                        deleted +
                        "</td>\
                                    </tr>";
                    $("#form-order-detail").append(html);
                });

                getTotal(orderId, total);
                getAll(orderId);
            },
            error: function(error) {},
        });
    }

    function getOrderDetailFix() {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        var orderId = "{{ $order->id }}";
        $('#qty-update').editable({
            type: 'text',
            title: 'Enter username',
            url: '/post',
            display: function(value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = $('<div>').html('<a href="#">' + value + '</a>');
                $(this).html(html);
            }
        });
        $.ajax({
            url: "{{ url('/api/v1/orderdetail/data/fix') }}" + "/" + orderId,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                var data = result.order_detail;
                var total = 0;
                $("#form-order-detail-fix").hide();
                $("#form-order-detail").empty();
                data.forEach((orderDetail) => {
                    total +=
                        parseFloat(orderDetail.price) *
                        parseFloat(orderDetail.qty);
                    if (total !== 0) {
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                        $("#sum").text(toRp(total));
                    } else {
                        $("#sum").text(total);
                    }
                    let totalFixed = "{{ $order->total }}";
                    var deleted = '';
                    var html =
                        "<tr>\
                                    <td>" +
                        orderDetail.code +
                        "</td>\
                                    <td>" +
                        orderDetail.name +
                        "</td>\
                                    <td>" +
                        orderDetail.description +
                        "</td>\
                        <td class='total desc'> <span><a href='#' id='qty-update' data-type='text' data-pk='" + orderDetail.id + "' data-name='qty' data-url='{{ url('api/v1/orderdetail/update/qty/" + orderDetail.id + "')}}'>" + orderDetail.qty + "</a></span></td> \
                        <td class='total'>" +
                        toRp(orderDetail.price) +
                        "</td>" +
                        deleted +
                        "</td>\
                                    </tr>";
                    $("#form-order-detail-fix").append(html);
                });

                getTotal(orderId, total);
                getAll(orderId);
            },
            error: function(error) {},
        });
    }

    function getTotal(id, total) {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: "{{  url('/web/order/update-total') }}" + "/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                var orderId = result.order.id;
                // document.getElementById("iddata").value = orderId;
                // document.getElementById("totaldata").value = total;
                // updateTotal(id, total);
            },
            error: function(error) {},
        });
    }

    function postOrderDetail(productId, qty) {
        var orderId = "{{ $order->id }}";
        var product_id = productId;
        var order_id = orderId;
        var price = $("#price").val();
        var qty = $("#input").val();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: "{{ route('order.detail-store') }}",
            type: "POST",
            dataType: "JSON",
            data: {
                product_id: product_id,
                order_id: order_id,
                qty: qty,
                price: price,
                CSRF_TOKEN,
            },
            success: function(result) {
                // console.lo(result);
                var html =
                    '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Keranjang ditambahkan\
                                            </p>\
                                        </div>\
                                    </div>';
                $("#status").html(html).show();
                $("#qty").val("");
                $("#filter-product").val("");
                getOrderDetail();
            },
            error: function(error) {
                console.log(error);
            },
        });
    }

    function getAll(id) {
        $.ajax({
            url: "{{ url('/api/v1/order/order-all') }}" + "/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                if (typeof result.order === "undefined") {
                    $("#form-all-div").hide();
                    $("#form-total-div").attr("class", "col-sm-12 col-xs-12");
                } else {
                    if (result.order[0].status === 0 && result.order[0].total === 0) {
                        resultTotal =
                            "<span class='badge badge-info'>Order</span></td>";
                        $("#save").removeAttr("disabled", "disabled");
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                    } else if (result.order[0].status === 0 && result.order[0].total !== 0) {
                        resultTotal = "<span class='badge badge-warning'>Order Transaction</span></td>";
                        $("#save").removeAttr("disabled", "disabled");
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                    } else {
                        resultTotal =
                            "<span class='badge badge-success'>Paid</span></td>";
                        $("#save").attr("disabled", "disabled");
                    }
                    var html =
                        "<tr>\
                    <td>" +
                        result.order[0].invoice +
                        "</td>\
                    <td>" +
                        result.order[0].customer.name +
                        "</td>\
                    <td>" +
                        toRp(result.order[0].total) +
                        "</td>\
                    <td>" +
                        resultTotal +
                        "</td>\
                    </tr>";
                    // result.order[0]
                    $("#form-all").html(html).show();
                    $("#form-all-div").show();
                }
            },
            error: function(error) {
                console.error();
            },
        });
    }

    $('#invoice').click(function() {
        $('#invoice-tabs').attr("style", "display:");
        $('#back').attr("style", "display:");
        $('#detail-invoice').attr("style", "display:none");
    })

    $('#back').click(function() {
        $('#invoice-tabs').attr("style", "display:none");
        $(this).attr("style", "display:none");
        $('#detail-invoice').attr("style", "display:");
    })
</script>
@endpush