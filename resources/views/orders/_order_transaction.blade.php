@extends('layouts.index') @section('content-index')
<?php $menu = 'Orders Transaction' ?>
<div class="page-header">
    <h1>
        {{ $menu }}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{ $menu }}
        </small>
    </h1>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            {!! Former::date('date')->name('r]') ->placeholder('PO Pesanan (hanya untuk repeat order yang belum punya SO!)')->label('PO Pesanan') ->addGroupClass('has-warning special-field') !!}
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Former::textarea('Casir') ->rows(5) ->value('MS. Office') ->columns(10) ->autofocus(); !!}
                                        </div>
                                        <div class="col-sm-12">
                                            {!! Former::select('customer') ->name('profiles-thread')->id('profiles-thread') ->data_placeholder('Customer')->placeholder('Pilih Customer P') ->label('Customer') ->addClass('select2-ajax-data') ->options(['Pilih Customer']) ->data_minimum_input_length('1')
                                            ->removeClass('form-control') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->value($product_id) ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                            </div>
                                            <div class="col-sm-4" style="margin-top: 25px;">
                                                <input type="text" class="btn btn-primary" value="Barcode">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        {!! Former::text('job.total_weight')->name('job[total_weight]') ->placeholder('Code')->label('Code') ->addClass('automoney') ->append('kg') ->required() ->value($product_id) ->addGroupClass('has-warning special-field') !!}
                                    </div>
                                    <br>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="panel">
                                                <div class="panel-body">
                                                    {{-- <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('11', 'C39')}}" alt="barcode" /> --}}
                                                    <img src="data:image/png;base64, {{ DNS1D::getBarcodePNG('4', 'C39+',3,33) }}" alt="barcode" width="100%" /> {{-- {!! DNS1D::getBarcodeSVG('4445645656', 'PHARMA2T',3,33,'green', true); !!} --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="pull-left">
                                            <h1>Invoice</h1>
                                        </div>
                                        <div class="pull-right">
                                            <h1> 098923KMBSU2001</h1>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <h1>
                                                <strong> Rp. 4.000.000, 00</strong>
                                            </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered" id="cart-table">
                            <thead>
                                <tr>
                                    <th>Barcode</th>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="10%">
                                        <img src="data:image/png;base64, {{ DNS1D::getBarcodePNG('4', 'C39+',3,33) }}" alt="barcode" />
                                    </td>
                                    <td>Shampo Merek Abal Abal PT Example</td>
                                    <td>4</td>
                                    <td>Rp. {{ number_format(10000, 2, '.', ',')}}</td>
                                    <td width="5%">
                                        <div class="center">
                                            <button class="btn btn-xs btn-primary"> <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-xs btn-danger"> <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12" style="margin-top: 20px;">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        {!! Former::textarea('Notes') ->rows(5) ->columns(10) ->autofocus(); !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                    </div>
                                    <div class="col-sm-12">
                                        {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                    </div>

                                    <div class="col-sm-12">
                                        {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Cash Pis')->label('Cash Pis') ->append('Pis') !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Sub Change Pis')->label('Sub Change Pis') ->append('Pis') !!}
                                            </div>
                                        </div>
                                        <div class="space"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <button class="btn btn-primary" width="100%">Payment</button>
                                                    <button class="btn btn-warning" width="100%">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    jQuery(function($) {
        $('#cart-table').DataTable();
        $("#profiles-thread").select2({
            theme: "bootstrap"
        });
        $('#casir').select2({
            theme: "bootstrap"
        });
        CustomerSelect();

        function CustomerSelect() {
            $.ajax({
                type: "POST",
                url: "{{ route('customer-detail-api') }}",
                success: function(result) {
                    var data = result.customer;
                    data.forEach(customer => {
                        $("#profiles-thread").select2({
                            theme: "bootstrap"
                        });
                        $("#profiles-thread").append("<option value='" + customer.id + "'>" + '#' + customer.id + '#' + customer.customer_name + "</option>");
                    });
                },
                async: false
            })
        }
    })
</script>
@endpush