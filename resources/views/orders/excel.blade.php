@extends('layouts.index') @section('content-index')
<?php $menu = 'Orders' ?>
<div class="page-header">
    <h1>
        {{ $menu }}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{ $menu }}
        </small>
    </h1>
</div>
<div class="widget-body">
    <div class="widget-main padding-6 no-padding-left no-padding-right">
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="row input-daterange">
                        <div class="col-md-5">
                            <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" />
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" />
                        </div>
                        <!-- <div class="col-md-2">
                            <button type="button" name="filter" id="filter" class="btn btn-primary"> <span
                                    style="cursor: pointer;" class="fa fa-filter"></button>
                            <button type="button" name="refresh" id="refresh" class="btn btn-primary"><span
                                    style="cursor: pointer;" class="fa fa-refresh fa-spin"></span></button>
									<button class="btn btn-sm btn-success">
										<a class="white" href="{{ route('order-web-export', ['from_date' => request()->input('from_date'), 'to_date' =>  request()->input('to_date')]) }}" id="export-to-excel"><span
											style="cursor: pointer;" class="fa fa-file-excel-o"></a></button>
						</div> -->
                        <div class="col-sm-2">
                            <div class="pull-left">
                                <button type="button" name="filter" id="filter" class="btn btn-sm btn-primary"> <span
									style="cursor: pointer;" class="fa fa-filter"></button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-sm btn-primary"><span
									style="cursor: pointer;" class="fa fa-refresh fa-spin"></span></button>

                                <button class="btn btn-sm btn-success">
									<a href="{{ route('order-download', ['from_date' => request()->input('from_date'), 'to_date' =>  request()->input('to_date')]) }}"   class="white" id="export-to-excel"><span
                                        style="cursor: pointer;" class="fa fa-file-excel-o"></a></button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <table id="order-table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Invoice</th>
                                                <th>Customer</th>
                                                <th>Customer Adress</th>
                                                <th>Product</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    jQuery(function($) {
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'dd.mm.yyyy',
            autoclose: true
        });
        fetch_data();

        function fetch_data(from_date = '', to_date = '') {

            var _token = $('input[name="_token"]').val();
            jQuery(function($) {
                var t = $("#order-table").DataTable({
                    processing: true,
                    serverSide: true,
                    bAutoWidth: true,
                    // dom: 'rti',
                    ajax: {
                        url: "{{ route('order-api-excel') }}",
                        data: {
                            from_date: from_date,
                            to_date: to_date
                        }
                    },
                    columns: [{
                        data: "invoice",
                        name: "invoice",
                    }, {
                        data: "customer.name",
                        name: "customer.name",
                    }, {
                        data: "customer.address",
                        name: "customer.address",
                    }, {
                        data: "total",
                        name: "total",
                        render: $.fn.dataTable.render.number('.', ',', 2)
                    }, {
                        data: "status",
                        name: "status",
                    }, {
                        data: "created_at",
                        name: "created_at",
                        render: function(d) {
                            return moment(d).format("dddd, DD-MM-YYYY");
                        },
                    }],
                    order: [
                        [0, "ASC"]
                    ],
                    pageLength: 15,
                    select: {
                        style: "multi",
                    },
                });
            });
        }


        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                // console.log(from_date, to_date);
                $('#order-table').DataTable().destroy();
                fetch_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#order-table').DataTable().destroy();
            fetch_data();
        });


        jQuery(document).on('change', '#from_date,#to_date', function(e) {
            var url = '{{ route("order-api-excel") }}';
            var params = {
                'from_date': $('#from_date').val(),
                'to_date': $('#to_date').val()
            };
            // console.log(params);
            var excel_url = '{{ route("order-download") }}';
            $('#export-to-excel').attr('href', excel_url + "?" + jQuery.param(params));
        });



    });
</script>
@endpush