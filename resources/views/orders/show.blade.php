@extends('layouts.index')
@section('content-index')
<div class="clearfix">
    <div class="pull-right tableTools-container"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-6">
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#home">
                                <i class="green ace-icon fa fa-home bigger-120"></i>
                                Product Detail
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            @include('orders._each_table_customer')
                        </div>

                        <div id="messages" class="tab-pane fade">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="tab-category">
                        <li class="active">
                            <a data-toggle="tab" href="#home1">
                                <i class="green ace-icon fa fa-home bigger-120"></i>
                                Category Detail
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="home1" class="tab-pane fade in active">
                            @include('orders._each_table_user')
                        </div>

                        <div id="messages1" class="tab-pane fade">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
        <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
        <div class="col-xs-12 col-sm-12">
            <div class="tabbable">
                <ul class="nav nav-tabs" id="tab-category">
                    <li class="active">
                        <a data-toggle="tab" href="#home1">
                            <i class="green ace-icon fa fa-home bigger-120"></i>
                            Show Detail Order
                        </a>
                    </li>
                </ul>
    
                <div class="tab-content">
                    <div id="home1" class="tab-pane fade in active">
                        <table class="table table-bordered">
                            <thead>
                                <th>Nomor</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection