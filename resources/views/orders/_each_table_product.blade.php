<?php $menu = 'Customer'; ?>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="table-header">Result {{ $menu }}</div>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <select class="select2 from-control" id="filter-product" style="width: 100% !important">
            <option value="Another">-- Search Selected Product --</option>
        </select>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="scrollit">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Prodcut Name</th>
                        <th>description</th>
                        <th>Stock</th>
                        <th>Barcode</th>
                    </tr>
                </thead>
                <tbody id="form-product"></tbody>
            </table>
        </div>
    </div>
</div>

@push('js')
<script type="text/javascript">
    $(document).ready(function() {
        ProductSelect();
        $("#filter-product").select2({
            theme: "bootstrap"
        });
        getProduct();
    });

    function ProductSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('product-detail-api') }}",
            success: function(result) {
                var data = result.product;
                data.forEach((product) => {
                    $("#filter-product").append(
                        "<option title='" +
                        product.id +
                        "' value='" +
                        product.name_product +
                        "'>" +
                        product.name_product +
                        "</option>"
                    );
                });
            },
            async: false,
        });
    }

    getProduct();

    function getProduct(product = "") {
        $.ajax({
            type: "POST",
            url: "{{ route('product-multiple') }}" + "?key=" + product,
            success: function(result) {
                var data = result.product;
                // console.log(data);
                $("#form-product").empty();
                data.forEach((product) => {
                    var html =
                        "<tr>\
                    <td>" +
                        product.name +
                        "</td>\
                    <td>" +
                        product.description +
                        "</td>\
                    <td>" +
                        product.stock +
                        "</td>\
                        <td><svg id='barcodeshow' jsbarcode-format='upc' jsbarcode-value='123456789012' jsbarcode-textmargin='0' jsbarcode-fontoptions='bold'></svg></td>\
                    </tr>";
                    $("#form-product").append(html);
                    JsBarcode("#barcodeshow", product.name);
                });
            },
            async: false,
        });
    }
    getProductIdSelect2();

    function getProductIdSelect2() {
        $("#filter-product").change(function() {
            let productrFilter = $(this).val();
            if (productrFilter != "") {
                getProduct(productrFilter);
                getProductDetail(productrFilter);
            } else {
                getProduct();
            }
        });

        $("#save").click(function() {
            let productId = $("#filter-product")
                .children("option:selected")
                .attr("title");
            postOrderDetail(productId, $("#qty").val());
        });
    }
</script>
@endpush