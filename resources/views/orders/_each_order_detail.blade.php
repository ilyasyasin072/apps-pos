<div id="status"></div>
<div class="space"></div>
<div class="col-sm-12 col-xs-12">
    <div class="row">
        <div class="col-sm-12" id="form-all-table" style="visibility:hidden">
            <table class="table table-bordered" id="table-form-all">
                <thead>
                    <th style="width: 10%;">Barcode</th>
                    <th style="width: 20%;">Kode Product</th>
                    <th style="width: 10%;">Name Product</th>
                    <th style="width: 5%;">Qty</th>
                    <th style="width: 10px;">Price Pcs</th>
                    <th style="width: 10px;"></th>
                    <th>Action</th>
                </thead>
                <tbody id="form-order-detail"></tbody>
            </table>
        </div>
        <div class="col-sm-10" id="form-all-div" style="visibility:hidden">
            <div>
                <table class="table table-bordered">
                    <tbody>
                        <td>Invoice</td>
                        <td>Customer</td>
                        <td>total</td>
                        <td>status</td>
                    </tbody>
                    <tbody id="form-all"></tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-2" id="form-total-div">
            <div class="pull-right" id="form-total" style="visibility:hidden">
                <table class="table table-bordered table-stripped">
                    <thead></thead>
                    <tbody>
                        <td class="desc">
                            <div>
                                <p>Total Keranjang</p>
                            </div>
                        </td>
                        <td class="desc">
                            <div>
                                <strong>
                                    <p id="sum"></p>
                                </strong>
                            </div>
                        </td>
                        <tr>
                            <td colspan="2" style="display: none" id="statusalert">
                                <div id="status1"></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="desc">
                                <button class="btn btn-xs btn-success" onclick="updateTotal()" id="btn-check-out">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                    Chekout Order
                                </button>
                            </td>
                            <td class="desc">
                                <button class="btn btn-xs btn-info" id="btn-invoice-out" style="display: none">
                                    <i class="fa fa-file-o"></i>
                                    invoice
                                </button>
                            </td>

                            <input type="text" id="iddata" style="display: none" />
                            <input type="text" id="totaldata" style="display: none" />
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>
                                    <h4>*Catatan</h4>
                                </strong>
                                <h5>
                                    Jika Sudah di Checkout items tidak bisa di hapus!!
                                </h5>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- <div class="row" style="dislpay: none" id="totaldiv">
        <div class="col-sm-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td>Total Bayar</td>
                        <td>
                            <div>
                                <input type="text" placeholder="" disabled>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Bayar</td>
                        <td>
                            <div>
                                <input type="text" placeholder="Nominal Bayar" data-a-sign="Rp. " data-a-dec="," data-a-sep=".">
                            </div>
                        </td>
                    </tr>
                    <tr style="display: none" class="total">
                        <td></td>

                        <td>

                            <div class="col-sm-6">
                                <h1 id="total_bayar"></h1>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <td>
                                <input id="transaction" type="submit" class="btn btn-sm btn-primary" value="Transaction">
                            </td>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div> -->
</div>

<div class="col-sm-12 col-xs-12 " style="margin-top: 20px; ">
    <div class="row ">

        <div class="col-sm-4 ">
            <div class="panel panel-default ">
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-sm-12 ">
                            {!! Former::textarea('Notes') ->rows(5) ->columns(10) ->autofocus(); !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div class="panel panel-default ">
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-sm-12 ">
                            {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                        </div>
                        <div class="col-sm-12 ">
                            {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                        </div>

                        <div class="col-sm-12 ">
                            {!! Former::text('job.pcs_qty') ->name('job[pcs_qty]')->id('job_pcs_qty') ->addClass('autointeger') ->placeholder('Total Pis')->label('Total Pis') ->append('Pis') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 ">
            <div class="panel panel-default ">
                <div class="panel-body ">
                    <div class="row ">
                        <div class="col-sm-12 ">
                            <div class="row ">
                                <div class="col-sm-12 ">
                                    <label for="">Total Payment</label>
                                    <input type="text" class="form-control" id="totalprice" data-a-sign="Rp. " data-a-dec="," data-a-sep="." readonly>

                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-sm-12 ">
                                    <label for="">Payment</label>
                                    <input type="text" class="form-control" id="bayar" placeholder="Rp. 1.000.000, 00">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1 id="total_bayar"></h1>
                                </div>
                            </div>
                            <div class="space "></div>
                            <div class="row ">
                                <div class="col-sm-12 ">
                                    <div class="pull-right ">
                                        <button class="btn btn-primary " id="transaction" width="100% ">Payment</button>
                                        <button class="btn btn-warning " width="100% ">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- MODAL UPDATE QTY --}}
<div class="modal fade form-modal-update" role="dialog" aria-labelledby="form-modal-update" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Update Qty</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <div id="alert_qty"></div>
                                <label for="inputQuotationPrice">ID</label>
                                <input type="text" placeholder="Code PRODUCT" id="modal-code" name="id" class="form-control" id="id-order-detail" readonly>
                                <label for="inputQuotationPrice">Code</label>
                                <input type="text" placeholder="ID" id="modal-id" class="form-control" id="id-order-detail">
                                <label for="inputQuotationPrice">Qty</label>
                                <input type="text" placeholder="Qty" name="qty" id="modal-qty" class="form-control">
                            </div>

                            <div class="card-footer text-right">
                                <input type="submit" onclick=Submit() class="btn btn-success" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js')
<script type="text/javascript">
    $('#table-form-all').DataTable();

    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split("").reverse().join("");
        var rev2 = "";
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== rev.length - 1) {
                rev2 += ".";
            }
        }
        return "Rp. " + rev2.split("").reverse().join("") + ",00";
    }

    $(document).ready(function() {
        getOrderDetail();
        getOrderDetailFix();
        $('#totalprice').autoNumeric('init');
        // checkTotal();
        let totalbayar = "{{ $order->total }}";
        let statusBayar = "{{ $order->status }}";
        $('#totalprice').val(toRp(totalbayar));

        if (statusBayar === 0) {
            $('#totaldiv').attr("style", "display:");
            // $('#table-form-all_wrapper').attr("style", "display:");
        } else {
            $('#detail-table').attr("style", "visibility:");
            $('#table-form-all_wrapper').attr("style", "display: none");
            $("#save").attr("disabled", "disabled");
        }

        // if (statusBayar == 1) {
        //     $('#transaction').attr("disabled", "disabled");
        // }
    });

    function myFunc(total, bayar) {
        return bayar - total;
    }


    $('#bayar').change(function() {
        let total = "{{ $order->total }}";
        let id = "{{ $order->id }}";
        let bayar = $(this).val();
        numbers = [total, bayar];
        let bayar1 = numbers.reduce(myFunc);
        var html = '<p>' + toRp(bayar1) + '</p>';
        $('#total_bayar').html(html).show();
        $('.total').attr("style", "display:");
    });

    $('#transaction').click(function() {
        checkTotal();
    })


    function checkTotal() {
        let checktotal = "{{ $order->total }}";
        let id = "{{ $order->id }}";
        let bayar1 = numbers.reduce(myFunc);
        if (checktotal == 0) {} else {
            $("#btn-check-out").attr("disabled", "disabled");
            $("#btn-invoice-out").removeAttr("style", true);
            $('#save').attr("style", "display:none");
        }
        getOrderDetail();

        $.ajax({
            url: "{{ url('web/order/status/bayar/')}}" + '/' + id,
            type: "POST",
            dataType: "JSON",
            data: {
                total_bayar: bayar1
            },
            success: function(result) {
                $('#totaldiv').attr("style", "display:none");
                window.location.href = '/web/order/' + id + '/transaction/paid';
                console.log('redirect');
            }
        })
    }

    function deleteOrderDetail(id) {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: "{{ url('/web/orderdetail/delete/') }}" + "/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                // console.log(result);
                var html =
                    '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Keranjang dihapus\
                                            </p>\
                                        </div>\
                                    </div>';
                $("#status").html(html).show();
                getOrderDetail();
            },
            error: function(error) {},
        });
    }

    function getOrderDetail() {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        var orderId = "{{ $order->id }}";
        $('#qty-update').editable({
            type: 'text',
            title: 'Enter username',
            url: '/post',
            display: function(value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = $('<div>').html('<a href="#">' + value + '</a>');
                $(this).html(html);
            }
        });
        $.ajax({
            url: "{{ url('/api/v1/orderdetail/data/') }}" + "/" + orderId,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                var data = result.order_detail;
                var total = 0;
                $("#form-order-detail1").hide();
                $("#form-order-detail").empty();
                data.forEach((orderDetail) => {
                    total +=
                        parseFloat(orderDetail.price) *
                        parseFloat(orderDetail.qty);
                    if (total !== 0) {
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                        $('#table-form-all_wrapper').attr("style", "display:");
                        $("#sum").text(toRp(total));
                        $('#total-harga').text(toRp(total));
                        $('#totalprice').val(total);
                    } else {
                        $("#sum").text(total);
                        $('#total-harga').text(toRp(total));
                        $('#totalprice').val(total);
                    }
                    // console.log(total);
                    let totalFixed = "{{ $order->total }}";
                    if (totalFixed == 0) {
                        deleted = '<td width="5%">\
                                        <div class="center">\
                                            <button class="btn btn-xs btn-primary edit-item" data-item-id=' + orderDetail.detail_id + '> <i class="fa fa-edit"></i>\
                                            </button>\
                                            <button class="btn btn-xs btn-danger" onclick=deleteOrderDetail(' +
                            orderDetail.detail_id +
                            ')> <i class="fa fa-trash"></i>\
                                            </button>\
                                        </div>\
                                    </td>'
                    } else {
                        deleted = '';
                    }
                    var code = "<svg class='barcode'></svg>";
                    var getid = "{{ url('/api/v1/orderdetail/update/qty/')}}" + '/' + orderDetail.detail_id;
                    var html =
                        "<tr class='data-row'>\
                        <td class='code'>" + code + "</td>\
                                    <td>" +
                        orderDetail.code +
                        "</td>\
                                    <td>" +
                        orderDetail.name +
                        "</td>\
                        <td class='total desc qty-get'>" + orderDetail.qty + "</td>\
                        <td class='total'>" +
                        toRp(orderDetail.price) +
                        "</td>\
                        <td class='total'>" + toRp(parseFloat(orderDetail.price) * parseFloat(orderDetail.qty)) + "</td>\
                        " + deleted + "</tr>";
                    $("#form-order-detail").append(html);

                    JsBarcode(".barcode", orderId, {
                        //   format: "pharmacode",
                        lineColor: "#0aa",
                        width: 1,
                        height: 40,
                        //   displayValue: false
                    });
                    // $('.qty-update').editable({
                    //     type: 'text',
                    //     title: 'Enter username',
                    //     url: '/post',
                    //     display: function(value) {
                    //         if (!value) {
                    //             $(this).empty();
                    //             return;
                    //         }
                    //         var html = $('<div>').html('<a href="#">' + value + '</a>');
                    //         $(this).html(html);
                    //         total =
                    //             parseFloat(orderDetail.price) *
                    //             parseFloat(value);
                    //         $("#sum").text(toRp(total));
                    //         $('#total-harga').text(toRp(total));
                    //         $('#totalprice').val(total);
                    //     }
                    // });

                });
                $('.edit-item').click(function() {
                    $(this).addClass('edit-item-trigger-clicked-pur');
                    var options = {
                        'backdrop': 'static'
                    };
                    $('.form-modal-update').modal(options)
                })


                $('.form-modal-update').on('show.bs.modal', function(event) {
                    var el = $(".edit-item-trigger-clicked-pur");
                    var row = el.closest(".data-row");
                    var id = el.data('item-id');

                    var qty = row.children(".qty-get").text();
                    var code = row.children(".code").text();
                    $("#modal-qty").val(qty.trim());
                    $('#modal-id').val(id);
                    $('#modal-code').val(code);
                    $('#modal-id').attr("disabled", "disabled");
                })
                $('.form-modal-update').on('hidden.bs.modal', function() {
                    $('.edit-item-trigger-clicked-pur').removeClass('edit-item-trigger-clicked-pur')
                    $("#pur-form").trigger("reset");
                })
                getTotal(orderId, total);
                getAll(orderId);
            },
            error: function(error) {},
        });
    }

    function Submit() {
        let qty = $("#modal-qty").val();
        let id = $('#modal-id').val();
        var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
        $.ajax({
            url: "{{ url('/web/orderdetail/update/qty/') }}" + '/' + id,
            type: "POST",
            // dataType: "JSON",
            data: {
                qty: qty,
                _token: CSRF_TOKEN,
            },
            success: function(result) {
                var html_alert = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                            Qty Change' + result.orderDetail.qty + '\
                                            /PCS</p>\
                                        </div>\
                                    </div>';
                $('#alert_qty').html(html_alert).show();
                getOrderDetail();
                $('.form-modal-update').modal('toggle'); //or  $('#IDModal').modal('hide');
                return false;
            }
        });
    }


    function getOrderDetailFix() {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        var orderId = "{{ $order->id }}";
        $('#qty-update').editable({
            type: 'text',
            title: 'Enter username',
            url: '/post',
            display: function(value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = $('<div>').html('<a href="#">' + value + '</a>');
                $(this).html(html);
            }
        });
        $.ajax({
            url: "{{ url('/api/v1/orderdetail/data/fix') }}" + "/" + orderId,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                var data = result.order_detail;
                var total = 0;
                $("#form-order-detail-fix").hide();
                $("#form-order-detail").empty();
                data.forEach((orderDetail) => {
                    total +=
                        parseFloat(orderDetail.price) *
                        parseFloat(orderDetail.qty);
                    if (total !== 0) {
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                        $("#sum").text(toRp(total));
                    } else {
                        $("#sum").text(total);
                    }
                    let totalFixed = "{{ $order->total }}";
                    if (totalFixed == 0) {
                        deleted =
                            '<td style="text-align: center;"><a class="red" href="#" onclick=deleteOrderDetail(' +
                            orderDetail.detail_id +
                            ')><i class="ace-icon fa fa-trash-o bigger-120"></i></a></td>';
                    } else {
                        deleted = '';
                    }
                    var html =
                        "<tr>\
                                    <td>" +
                        orderDetail.code +
                        "</td>\
                                    <td>" +
                        orderDetail.name +
                        "</td>\
                                    <td>" +
                        orderDetail.description +
                        "</td>\
                        <td class='total desc'> <span><a href='#' id='qty-update' data-type='text' data-pk='" + orderDetail.id + "' data-name='qty' data-url='{{ url('api/v1/orderdetail/update/qty/" + orderDetail.id + "')}}'>" + orderDetail.qty + "</a></span></td> \
                        <td class='total'>" +
                        toRp(orderDetail.price) +
                        "</td>" +
                        deleted +
                        "</td>\
                                    </tr>";
                    $("#form-order-detail-fix").append(html);
                });

                getTotal(orderId, total);
                getAll(orderId);
            },
            error: function(error) {},
        });
    }

    function getTotal(id, total) {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: "{{  url('/web/order/update-total') }}" + "/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                var orderId = result.order.id;
                document.getElementById("iddata").value = orderId;
                document.getElementById("totaldata").value = total;
                // updateTotal(id, total);
            },
            error: function(error) {},
        });
    }

    function updateTotal() {
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        let order_id = $("#iddata").val();
        let total = $("#totaldata").val();
        $.ajax({
            url: "{{  url('/web/order/post-total') }}" + "/" + order_id,
            type: "POST",
            dataType: "JSON",
            data: {
                CSRF_TOKEN,
                total: total,
            },
            success: function(result) {
                var html =
                    '<div class="widget-body" id="status" style="width: 100%;">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Jumlah Tagihan Anda' +
                    result.order.total +
                    "\
                                            </p>\
                                        </div>\
                                    </div>";
                $("#status1").html(html).show();
                $("#statusalert").removeAttr("style", true);
                $("#btn-check-out").attr("disabled", "disabled");
                $("#btn-invoice-out").removeAttr("style", true);
                $('#save').attr("style", "display:none");
                $("#form-product-search").hide();
                $("#form-product-desc").hide();
                $('#filter-product').attr("disabled", "disabled")
                $('#form-all-table').attr("style", "visibility:hidden");
                $('#transaction').removeAttr("disabled");
                $('#table-form-all_wrapper').attr("style", "display: none");
                getAll(order_id);
                getOrderDetail();
                // window.location.href = '/web/order/'+order_id+'/transaction';
            },
            error: function(error) {},
        });
    }

    function postOrderDetail(productId, qty) {
        var orderId = "{{ $order->id }}";
        var product_id = productId;
        var order_id = orderId;
        var price = $("#price").val();
        var qty = $("#input").val();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: "{{ route('order.detail-store') }}",
            type: "POST",
            dataType: "JSON",
            data: {
                product_id: product_id,
                order_id: order_id,
                qty: qty,
                price: price,
                CSRF_TOKEN,
            },
            success: function(result) {
                // console.lo(result);
                var html =
                    '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Keranjang ditambahkan\
                                            </p>\
                                        </div>\
                                    </div>';
                $("#status").html(html).show();
                $("#qty").val("");
                $("#filter-product").val("");
                getOrderDetail();
            },
            error: function(error) {
                console.log(error);
            },
        });
    }

    function getAll(id) {
        $.ajax({
            url: "{{ url('/api/v1/order/order-all') }}" + "/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                if (typeof result.order === "undefined") {
                    $("#form-all-div").hide();
                    $("#form-total-div").attr("class", "col-sm-12 col-xs-12");
                } else {
                    if (result.order[0].status === 0 && result.order[0].total === 0) {
                        resultTotal =
                            "<span class='badge badge-info'>Order</span></td>";
                        $("#save").removeAttr("disabled", "disabled");
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                    } else if (result.order[0].status === 0 && result.order[0].total !== 0) {
                        resultTotal = "<span class='badge badge-warning'>Order Transaction</span></td>";
                        $("#save").removeAttr("disabled", "disabled");
                        $("#form-total").attr("style", "visibility:");
                        $("#form-all-div").attr("style", "visibility:");
                        $('#form-all-table').attr("style", "visibility:");
                        $('#detail-table').attr("style", "visibility:");
                    } else {
                        resultTotal =
                            "<span class='badge badge-success'>Paid</span></td>";
                        $("#save").attr("disabled", "disabled");
                    }
                    var html =
                        "<tr>\
                    <td>" +
                        result.order[0].invoice +
                        "</td>\
                    <td>" +
                        result.order[0].customer.name +
                        "</td>\
                    <td>" +
                        toRp(result.order[0].total) +
                        "</td>\
                    <td>" +
                        resultTotal +
                        "</td>\
                    </tr>";
                    // result.order[0]
                    var inv = '<h3>' + result.order[0].invoice + '</h3>';
                    $('#inv-show').html(inv).show();
                    $("#form-all").html(html).show();
                    $("#form-all-div").show();
                }
            },
            error: function(error) {
                console.error();
            },
        });
    }
</script>
@endpush