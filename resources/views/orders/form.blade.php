<div class="row">
    <div class="">
        <!-- PAGE CONTENT BEGINS -->
       <div class="col-sm-12">
           <div class="row">
            <div class="col-xs-12 col-sm-12" id="form-customer">
                <div class="widget-box">
                    <div class="widget-header">
                        <?php $menus = 'Form order'; ?>
                        <h4 class="widget-title">{{$menus}}</h4>
    
                        <span class="widget-toolbar">
                            <a href="#" data-action="settings">
                                <i class="ace-icon fa fa-cog"></i>
                            </a>
    
                            <a href="#" data-action="reload">
                                <i class="ace-icon fa fa-refresh"></i>
                            </a>
    
                            <a href="#" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
    
                            <a href="#" data-action="close">
                                <i class="ace-icon fa fa-times"></i>
                            </a>
                        </span>
                    </div>
    
                    <div class="widget-body">
                        <div class="widget-main">
                            @if(!isset($order))
                            <form class="form-horizontal" role="form" enctype="multipart/form-data" id="ajax">
                                @else
                                <form class="form-horizontal" role="form" action="" enctype="multipart/form-data">
                                    @endif
                                    <div class="space-4"></div>
                                    <input type="text" id="nextId" value="{{$nextId}}" hidden>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right" for="form-field-2"
                                            hidden="">Invoice</label>
                                        <div class="col-sm-4">
                                            <input type="text" id="form-field-2" name="invoice" class="invoice"
                                                value="{{ !isset($order->invoice) ? '' : $order->invoice }}"
                                                class="col-xs-10 col-sm-5" hidden="" />
                                        </div>
                                    </div>
    
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label no-padding-right"
                                            for="form-field-2">Customer</label>
                                        <div class="col-sm-4">
                                            <select class="" name="customer_id" id="customer">
                                                <option>--Select Category--</option>
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="space-4"></div>
    
                                    <div class="space-4"></div>
    
                                    <div class="form-group" hidden>
                                        <label class="col-sm-5 control-label no-padding-right">
                                            Total
                                        </label>
                                        <div class="col-sm-2">
                                            <input type="text" class="total" name="total"
                                                class="col-xs-10 col-sm-5 form-control" id="form-total" data-a-sign="Rp. "
                                                data-a-dec="," data-a-sep="."
                                                value="{{ !isset($order->total) ? '' : $order->total }} " required />
                                            <span class="help-inline col-xs-12 col-sm-7">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-5 col-md-9">
                                            <button class="btn btn-info" type="submit">
                                                <i class="ace-icon fa fa-check bigger-110"></i>
                                                Submit
                                            </button> &nbsp; &nbsp; &nbsp;
                                            <button class="btn" type="reset">
                                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                   
                </div>
            </div>
            <div class="col-xs-12 col-sm-6" id="form-detail-show" style="display:none">
                <div class="widget-box">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title smaller">Definition List</h4>
    
                        <div class="widget-toolbar">
                            <label>
                                <small class="green">
                                    <b>Horizontal</b>
                                </small>
    
                                <input id="id-check-horizontal" type="checkbox"
                                    class="ace ace-switch ace-switch-6" />
                                <span class="lbl middle"></span>
                            </label>
                        </div>
                    </div>
    
                    <div class="widget-body">
                        <div class="widget-main">
                            <code class="pull-right" id="dt-list-code">&lt;dl&gt;</code>
    
                            <div id="form-detail"></div>
                        </div>
                    </div>
                </div>
            </div>
           </div>
       </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        CustomerSelect();
        $("#customer").select2({
            theme: "bootstrap"
        });
        UserSelect();
        $("#user").select2({
            theme: "bootstrap"
        });
        $('#alert').hide();

        $('#form-total').autoNumeric('init');
    });


    
    $('#customer').change(function(){
        let value = $(this).val();
        CustomerGet($(this).val());
        if(value === '0') {
            $('#form-detail-show').attr('style','display:none');
            $('#form-customer').removeClass( "col-sm-6" ).addClass( "col-sm-12" );
        } 
    })


    function CustomerSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('customer-detail-api') }}",
            success: function(result) {
                var data = result.customer;
                data.forEach(customer => {
                    $("#customer").append("<option value='" + customer.id + "'>" + ' Customer : ' + customer.customer_name + "</option>");
                });
            },
            async: false
        })
    }
    function CustomerGet(id){
        $.ajax({
            url: "{{ url('/api/v1/customer/detail') }}" +'/'+id,
            type: "POST",
            dataType: "JSON",
            success: function(result) {
                let html = '<dl id="dt-list-1"><dt>Email</dt>\
                                            <dd>'+result.email+'</dd>\
                                            <dt>Name</dt>\
                                            <dd>'+result.name+'</dd>\
                                            <dt>Address</dt>\
                                            <dd>'+result.address+'</dd>\
                                            <dt>Phone</dt>\
                                            <dd>'+result.phone+'</dd></dl>';
                $('#form-detail').html(html).show();
                $('#form-detail-show').attr('style','display:');
                $('#form-customer').removeClass( "col-sm-12" ).addClass( "col-sm-6" );
            }
        })
    }

    function UserSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('user-detail-api') }}",
            success: function(result) {
                var data = result.user;
                data.forEach(user => {
                    $("#user").append("<option value='" + user.id + "'>" + ' User : ' + user.user_name + "</option>");
                });
            },
            async: false
        })
    }

    jQuery(function($) {
        $('#ajax').submit(function(event) {
            let nextId = $('#nextId').val();
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "{{ route('order-web-store') }}",
                dataType: "json",
                data: $('#ajax').serialize(),
                success: function(data) {
                    var alert = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Berhasil Di simpan\
                                            </p>\
                                        </div>\
                                    </div>';
                    // $('#alert').html(alert).show();
                    // $("#example").DataTable().ajax.reload();
                    $('.invoice').val('');
                    $('.total').val('');
                    window.location.href = '/web/order/' + nextId + '/transaction';
                },
                error: function(error) {
                    var alert = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-danger">\
                                           ' + error.responseJSON.error + '\
                                            </p>\
                                        </div>\
                                    </div>';
                    $('#alert').html(alert).show();
                }
            });
        })
    });
</script>