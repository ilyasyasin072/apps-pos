@extends('layouts.index') @section('content-index')
<?php $menu = 'Pending Order' ?>
<div class="page-header">
    <h1>
        {{ $menu }}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{ $menu }}
        </small>
    </h1>
</div>
@include('categories._session')

<div id="alert"></div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">Result {{ $menu }}</div>
        <div>
            {{-- <table id="order-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>invoice</th>
                        <th>total</th>
                        <th>email</th>
                        <th>address</th>
                        <th>create</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table> --}}
            <div class="widget-header">
                <span class="widget-toolbar">
                    <a href="#" data-action="reload" id="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>
                </span>
            </div>
            <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th><input type="text" data-col-index="1" class="form-control datatable-column-search"
                                placeholder="Search Invoice" /></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Nomor Order</th>
                        <th>Invoice</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Create Date</th>
                        <th>Total Order</th>
                        <th>Status Paid</th>
                        <th>Actions</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nomor Order</th>
                        <th>Invoice</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Create Date</th>
                        <th>Total Order</th>
                        <th>Status Paid</th>
                        <th>Actions</th>
                        <th>Detail</th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Modal --}}

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Order Detail</h4>
            </div>
            <div class="modal-body">
                <div class="insertHere"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

@endsection @push('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#status_paid').select();
})
    function toRp(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
    }

    function showCreate() {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/order/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }

    function showEdit(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/order/edit" + "/" + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }

    $(document).ready(function() {
    var table = $('#example').DataTable({
            // hiding columns via datatable column.visivle API
            processing: true,
                serverSide: true,
                bAutoWidth: false,
                // dom: 'rti',
                ajax: "{{ route('order-panding') }}",
                "scrollY": true,
    	                    "scroller": 
                            {"loadingIndicator": true},
                search: {
                    caseInsensitive: true,
                },
                columns: [{
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "invoice",
                    name: "invoice",
                }, {
                    data: "customer.email",
                    name: "customer.email",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "customer.address",
                    name: "customer.address",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "created_at",
                    name: "created_at",
                    orderable: false,
                    searchable: false,
                    render: function(d) {
                        return moment(d).format("dddd, DD-MM-YYYY");
                    },
                }, {
                    data: "total",
                    name: "total",
                    orderable: false,
                    searchable: false,
                    render: $.fn.dataTable.render.number( '.', ',', 2 )
                },

                {
                    data: "status",
                    name: "status"
                },
                 {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                    className: "center"
                }, ],
                order: [
                    [0, "desc"]
                ],
                columnDefs: [
                        { targets: [0, 5], visible: false},
                    ],
                pageLength: 20,
                select: {
                    style: "multi",
                },
                    "columnDefs": [ {
                    "targets": 8,
                    "data": null,
                    "className": 'text-center',
                    "defaultContent": "<a href='#' id='modalshow'><span class='glyphicon glyphicon-leaf green' aria-hidden='true'></span></a>"
                    }],
                    initComplete: function () {
                            this.api().columns().every( function () {
                                var column = this;
                                if (column.index() === 6) {  //skip if column 0
                                    var select = $('<select class="form-control select2"><option value=""></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    } );
                
                                column.data().unique().sort().each( function ( d, j ) {
                                    select.append( '<option  class="select2" value="'+d+'">'+d+'</option>' )
                                } );
                                }
                            } );
                        }
            });

        $('#example tbody').on('click', '#modalshow', function() {
            var data = table.row($(this).parents('tr')).data(); // getting target row data
            // console.log(data);
            $('.insertHere').html(
                    // Adding and structuring the full data
            '<table class="table dtr-details" width="100%"><tbody><tr><td>Nomor<td><td>' + data.id + 
            '</td></tr><tr><td>Invoice<td><td>' + data.invoice + 
            '</td></tr><tr><td>Email<td><td>' + data.customer.email + 
            '</td></tr><tr><td>Address<td><td>' + data.customer.address +  
            '</td></tr><tr><td>Total<td><td>' + toRp(data.total) + 
            '</td></tr></tbody></table>'
            );
            $('#myModal').modal('show'); // calling the bootstrap modal
        });

        $('.datatable-column-search').on('keydown', function() {
          table
          .column($(this).data('colIndex'))
          .search(this.value, false, true, false)
          .draw();
      });

});

// function autoRefreshData() {
//         $("#order-table").DataTable().ajax.reload();
//     }
// setInterval("autoRefreshData()", 10000);

    $('#reload').click(function(){
        $("#example").DataTable().ajax.reload();
    })

    function onDelete(id) {
        const getId = id;
        $.ajax({
            type: "GET",
            url: "/web/order/delete" +'/'+ getId,
            dataType: "JSON",
            success: function(result) {
                $("#example").DataTable().ajax.reload();
                var html = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                            Delete Berhasil\
                                            </p>\
                                        </div>\
                                    </div>';
            $('#alert').html(html).show();
            }, 
            error: function(error) {
                var html = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-warning">\
                                           '+error.responseJSON.error+'\
                                            </p>\
                                        </div>\
                                    </div>';
            $('#alert').html(html).show();
            }
        })
    }
</script>
@endpush