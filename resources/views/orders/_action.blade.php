
@if($order->status != 1)
<span class="blue">
        <a href="{{ route('order-transaction', ['id' => $order->id])}}"><i class="ace-icon fa fa-search-plus bigger-120"></i></a>
</span>
@else
<span class="blue">
        <a href="{{ route('order-transaction-paid', ['id' => $order->id])}}"><i class="ace-icon fa fa-search-plus bigger-120"></i></a>
</span>
@endif
{{-- <span class="green">
   <i class="ace-icon fa fa-pencil-square-o bigger-120" ></i>
</span> --}}
@if($order->status != 1)
<span class="red">
    <a class="red" href="#" onclick="onDelete({{ $order->id }});"><i class="ace-icon fa fa-trash-o bigger-120"></i></a>
</span>
@endif