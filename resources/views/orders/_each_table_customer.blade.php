<?php $menu = 'Customer'; ?>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="table-header">Result {{ $menu }}</div>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <select class="select2 from-control" id="filter-customer" style="width:100%!important;" multiple="multiple">
        </select>
        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="scrollit">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Nama Customer</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody id="form-customer"></tbody>
            </table>

        </div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">Next</a></li>
            </ul>
        </nav>
    </div>
</div>

@push('js')
<script type="text/javascript">
    $(document).ready(function(){
        CustomerSelect();
        $('#filter-customer').select2();
        getCustomer();
    })

    function CustomerSelect() {
        $.ajax({
            type: "POST",
            url: "{{ route('customer-detail-api') }}",
            success: function(result) {
                var data = result.customer;
                data.forEach(customer => {
                    $("#filter-customer").append("<option value='" + customer.customer_name + "'>" + ' Customer : ' + customer.customer_name + "</option>");
                });
            },
            async: false
        })
    }
    
    getCustomer();

    function getCustomer(customer = '') {
        $.ajax({
            type: "POST",
            url: "{{ route('customer-multiple') }}" + '?key=' + customer,
            success: function(result) {
                var data = result.customer;
                // console.log(data);
                $('#form-customer').empty();
                data.forEach(customer => {
                    var html = '<tr>\
                    <td>'+customer.customer_name+'</td>\
                    <td>'+customer.email+'</td>\
                    </tr>';
                    $('#form-customer').append(html);
                });
                
            },
            async: false
        })

    }

    $('#filter-customer').change(function(){
        let customerFilter = $(this).val();
        if (customerFilter != '') {
            getCustomer(customerFilter);
        } else {
            getCustomer();
        }
    })
</script>
@endpush