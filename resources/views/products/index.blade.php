@extends('layouts.index') @section('content-index')
<?php $menu = 'Customers' ?>
<div class="page-header">
    <h1>
        {{ $menu }}
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            {{ $menu }}
        </small>
    </h1>
</div>
<div id="alert"></div>
<!-- /.page-header -->
<div class="">
    <div class="pull-right" style="margin-bottom: 10px">
        <button class="btn btn-sm btn-primary" onclick="showCreate();">
            <i class="ace-icon fa fa-plus bigger-120"></i>&nbsp;Create Customer
        </button>
        <button class="btn btn-sm btn-success" onclick="showCustomer();">
            <i class="ace-icon fa fa-refresh bigger-120"></i>&nbsp;Customer
        </button>
    </div>
</div>
<div id="form"></div>

<div class="row" id="datatable">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->

        <div class="table-header">Result {{ $menu }}</div>
        <table class="table table-bordered" id="customers-table">
            <thead>
                <tr>
                    <th>Nomor Products</th>
                    <th>Code Products</th>
                    <th>Descriptoin Products</th>
                    <th>Stock</th>
                    <th>Monitor Category</th>
                    <th>Create Date</th>
                    <th>Photo</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

@endsection @push('js')
<script type="text/javascript">
    jQuery(document).ready(function() {
        customers();
    });

    function customers() {
        jQuery(function($) {
            var t = $("#customers-table").DataTable({
                processing: true,
                serverSide: true,
                bAutoWidth: false,
                // dom: 'rti',
                ajax: "{{ route('product-api') }}", 
                            "scrollY": true,
    	                    "scroller": 
                            {"loadingIndicator": true},
                search: {
                    caseInsensitive: true,
                },
                columns: [{
                    data: "id",
                    name: "id",
                }, {
                    data: "code",
                    name: "code",
                }, {
                    data: "name",
                    name: "name",
                }, {
                    data: "description",
                    name: "description",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "category.name",
                    name: "category.name",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "created_at",
                    name: "created_at",
                    orderable: false,
                    searchable: false,
                    render: function(d) {
                        return moment(d).format("dddd, DD-MM-YYYY");
                    },
                }, {
                    data: "photo",
                    name: "photo",
                    orderable: false,
                    searchable: false,
                }, {
                    data: "action",
                    name: "action",
                    orderable: false,
                    searchable: false,
                }, ],
                order: [
                    [0, "desc"]
                ],
                pageLength: 15,
                select: {
                    style: "multi",
                },
                columnDefs: [{
                    targets: null,
                    visible: null,
                }, ],
            });
        });
    }

    function showCreate() {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/product/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data).show();
                $('#datatable').hide();
            },
        });
    }

    function showCustomer(){

        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
       if ($('#datatable').show()) {
           
        $.ajax({
            url: "/web/product/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data).hide();
            },
        });
       } else {

        $.ajax({
            url: "/web/product/create",
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data).show();
            },
        });
       }
    }

    function showEdit(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

        $.ajax({
            url: "/web/product/edit" + "/" + id,
            type: "GET",
            data: {
                CSRF_TOKEN,
            },
            success: function(data) {
                // console.log(data);
                $("#form").html(data);
            },
        });
    }

    function deleted(id) {
        event.preventDefault();
        const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");
        $.ajax({
            url: '/web/product/delete/' + id,
            type: "POST",
            dataType: "JSON",
            data: {
                CSRF_TOKEN,
            },
            success: function(result) {
                $("#customers-table").DataTable().ajax.reload();
                let html = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-success">\
                                           Data Berhasil di Hapus\
                                            </p>\
                                        </div>\
                                    </div>';
                $("#alert").html(html).show();
            },
            error: function(error) {
                // console.log(error.responseJSON.message);
                if (error.status == 500) {
                    let html = '<div class="widget-body" id="status">\
                                        <div class="widget-main">\
                                            <p class="alert alert-sm alert-warning">\
                                           ' + error.responseJSON.message + '\
                                            </p>\
                                        </div>\
                                    </div>';
                    $("#alert").html(html).show();
                }
            },
        });
    }
</script>
@endpush