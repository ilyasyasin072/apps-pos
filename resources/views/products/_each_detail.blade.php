@extends('layouts.index') @section('content-index')
<div class="clearfix">
    <div class="pull-right tableTools-container"></div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <table class="table table-bordered">
                <tbody>
                    <td>
                        <input type="text" placeholder="Search Product" class="input-search" />
                    </td>
                    <td>
                        <button class="btn btn-xs btn-primary" style="width: 100%" id="search">
                            Search
                        </button>
                    </td>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4"></div>
    </div>
    <div class="col-sm-12" id="form-product-show">
        @include('products._each_product')
    </div>
</div>
@endsection @push('js')
<script type="text/javascript">
    const CSRF_TOKEN = $('meta[name="csrf_token"]').attr("content");

    $(document).ready(function() {
        $("#form-product-show").show();
        getProduct();
    });

    $("#search").click(function() {
        let search = $(".input-search").val();
        $("#form-product-show").show();
        getProduct(search);
    });

    // $(".input-search").change(function() {
    //     getProduct($(this).val());
    // });

    $(".input-search").on("change blur keyup", function() {
        getProduct($(this).val());
    });

    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split("").reverse().join("");
        var rev2 = "";
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== rev.length - 1) {
                rev2 += ".";
            }
        }
        return "Rp. " + rev2.split("").reverse().join("") + ",00";
    }

    $("#category").change(function() {
        getProductCategory($(this).val());
    });

    function getProductCategory(category = "") {
        $.ajax({
            url: "{{ route('search-product') }}" + "?search=" + category,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                var data = result.product;
                // console.log(data);
                $("#detail-show").empty();
                data.forEach((e) => {
                    let id = e.id;
                    var html =
                        '<div class="col-xs-6 col-sm-4 col-md-3"><div class="thumbnail search-thumbnail">\
                <img src="/uploads/product/product-default.jpg" alt="" class="media-object"  />\
                <div class="caption">\
                    <div class="clearfix">\
                        <span class="pull-right label label-grey info-label">' +
                        toRp(e.price) +
                        '</span>\
                    </div>\
                    <h3 class="search-title">\
                        <a href="{{ url("/web/product/detail")}}'+'/'+id+'" class="blue">' +
                        e.name +
                        "</a>\
                    </h3>\
                    <p>\
                        " +
                        e.description +
                        "\
                    </p>\
                </div>\
            </div></div>";
                    $("#detail-show").append(html).show();
                });
            },
            error: function(error) {},
        });
    }

    function getProduct(search = "") {
        $.ajax({
            url: "{{ route('search-product') }}" + "?key=" + search,
            type: "GET",
            dataType: "JSON",
            success: function(result) {
                var data = result.product;
                // console.log(data);
                $("#detail-show").empty();
                data.forEach((e) => {
                    let id = e.id;
                    var html =
                        '<div class="col-xs-6 col-sm-4 col-md-3"><div class="thumbnail search-thumbnail">\
                <img src="/uploads/product/product-default.jpg" alt="" class="media-object"  />\
                <div class="caption">\
                    <div class="clearfix">\
                        <span class="pull-right label label-grey info-label">' +
                        toRp(e.price) +
                        '</span>\
                    </div>\
                    <h3 class="search-title">\
                        <a href="{{ url("/web/product/detail")}}'+'/'+id+'" class="blue">' +
                        e.name +
                        "</a>\
                    </h3>\
                    <p>\
                        " +
                        e.description +
                        "\
                    </p>\
                </div>\
            </div></div>";
                    $("#detail-show").append(html).show();
                });
            },
            error: function(error) {},
        });
    }
</script>
@endpush