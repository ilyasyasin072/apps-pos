<span class="blue">
    <i class="ace-icon fa fa-search-plus bigger-120"></i>
</span>
<span class="green" href="" onclick="showEdit({{ $product->id }})">
    <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
</span>
<span class="red">
    <!-- <a class="red" href="{{ route('product-web-delete', ['id' => $product->id]) }}"> -->
    <a class="red" href="#" onclick="deleted({{ $product->id }})">
        <i class="ace-icon fa fa-trash-o bigger-120"></i>
    </a>
</span>