<div class="row">
    <div class="">
        <!-- PAGE CONTENT BEGINS -->
        <div class="col-xs-12 col-sm-12">
            <div class="widget-box">
                <div class="widget-header">
                    <?php $menus = 'Form Product'; ?>
                    <h4 class="widget-title">{{$menus}}</h4>

                    <span class="widget-toolbar">
                        <a href="#" data-action="settings">
                            <i class="ace-icon fa fa-cog"></i>
                        </a>

                        <a href="#" data-action="reload">
                            <i class="ace-icon fa fa-refresh"></i>
                        </a>

                        <a href="#" data-action="collapse">
                            <i class="ace-icon fa fa-chevron-up"></i>
                        </a>

                        <a href="#" data-action="close">
                            <i class="ace-icon fa fa-times"></i>
                        </a>
                    </span>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        @if(!isset($product))
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('product-web-store') }}" enctype="multipart/form-data">
                            @else
                            <form class="form-horizontal" role="form" action="{{ route('product-web-update', ['id' => $product->id])}}" enctype="multipart/form-data">
                                @endif
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right" for="form-field-2">Product
                                        Code</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="form-field-2" name="code" value="{{ !isset($product->code) ? $generator_prodcutid : $product->code }}" class="col-xs-10 col-sm-5" required readonly/>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Upload Photo
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="file" name="photo" class="form-control">
                                        </span>
                                    </div>
                                </div>

                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right" for="form-field-2">Product
                                        Name</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="form-field-2" name="name" value="{{ !isset($product->name) ? '' : $product->name }}" class="col-xs-10 col-sm-5" required />
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Product Description
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="description" class="col-xs-10 col-sm-5" id="form-input-readonly" value="{{ !isset($product->description) ? '' : $product->description }} " required />
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        </span>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Stock
                                    </label>
                                    <div class="col-sm-2">
                                        <input type="number" name="stock" class="col-xs-10 col-sm-5" id="form-input-readonly" value="{{ !isset($product->stock) ? '' : $product->stock }} " required />
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        </span>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Price
                                    </label>
                                    <div class="col-sm-2">
                                        <input type="text" name="price" class="col-xs-10 col-sm-5" data-a-sign="Rp. " data-a-dec="," data-a-sep="." id="form-price" value="{{ !isset($product->price) ? '' : $product->price }} " required />
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label no-padding-right">
                                        Category
                                    </label>
                                    <div class="col-sm-4">
                                        <select class="col-xs-10 select2" name="category_id" id="category">
                                            <option>--Select Category--</option>
                                        </select> {{-- <input type="text" name="phone" class="col-xs-10 col-sm-5" id="form-input-readonly" value="{{ !isset($product->category) ? '' : $product->category->name }}
                                        " required /> --}}
                                        <span class="help-inline col-xs-12 col-sm-7">
                                        </span>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-5 col-md-9">
                                        <button class="btn btn-info" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Submit
                                        </button> &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        categorySelect();
        $("#category").select2({
            theme: "bootstrap"
        });
        $('#form-price').autoNumeric('init');
    });

    function categorySelect() {
        $.ajax({
            type: "GET",
            url: "{{ route('category-json-api') }}",
            success: function(data) {
                data.forEach(category => {
                    $("#category").append("<option value='" + category.id + "'>" + category.name + ' description : ' + category.description + "</option>");
                });
            },
            async: false
        })
    }

    jQuery(function($) {

        try {
            Dropzone.autoDiscover = false;

            var myDropzone = new Dropzone('#dropzone', {
                previewTemplate: $('#preview-template').html(),

                thumbnailHeight: 120,
                thumbnailWidth: 120,
                maxFilesize: 0.5,

                //addRemoveLinks : true,
                //dictRemoveFile: 'Remove',

                dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
				<span class="smaller-80 grey">(or click)</span> <br /> \
				<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',

                thumbnail: function(file, dataUrl) {
                    if (file.previewElement) {
                        $(file.previewElement).removeClass("dz-file-preview");
                        var images = $(file.previewElement).find("[data-dz-thumbnail]").each(function() {
                            var thumbnailElement = this;
                            thumbnailElement.alt = file.name;
                            thumbnailElement.src = dataUrl;
                        });
                        setTimeout(function() {
                            $(file.previewElement).addClass("dz-image-preview");
                        }, 1);
                    }
                }

            });


            //simulating upload progress
            var minSteps = 6,
                maxSteps = 60,
                timeBetweenSteps = 100,
                bytesPerStep = 100000;

            myDropzone.uploadFiles = function(files) {
                var self = this;

                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

                    for (var step = 0; step < totalSteps; step++) {
                        var duration = timeBetweenSteps * (step + 1);
                        setTimeout(function(file, totalSteps, step) {
                            return function() {
                                file.upload = {
                                    progress: 100 * (step + 1) / totalSteps,
                                    total: file.size,
                                    bytesSent: (step + 1) * file.size / totalSteps
                                };

                                self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                                if (file.upload.progress == 100) {
                                    file.status = Dropzone.SUCCESS;
                                    self.emit("success", file, 'success', null);
                                    self.emit("complete", file);
                                    self.processQueue();
                                }
                            };
                        }(file, totalSteps, step), duration);
                    }
                }
            }


            //remove dropzone instance when leaving this page in ajax mode
            $(document).one('ajaxloadstart.page', function(e) {
                try {
                    myDropzone.destroy();
                } catch (e) {}
            });

        } catch (e) {
            //   alert('Dropzone.js does not support older browsers!');
        }

    });
</script>