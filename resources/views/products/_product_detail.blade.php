@extends('layouts.index') @section('content-index')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="col-sm-4 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <img src="/uploads/product/product-default.jpg" alt="" width="100%" height="100%" />
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="widget-body" id="status">
                            <div class="widget-main">
                                <p class="alert alert-sm alert-success">
                                    <strong>Stok produk kosong</strong> <br> Untuk sementara produk ini tidak dijual. Silakan hubungi toko yang bersangkutan untuk informasi lebih lanjut.
                                </p>
                                <span class="label label-lg label-pink arrowed-right col-sm-12">Large</span>
                            </div>
                        </div>
                        <div class="widget-body">
                            <!-- <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="list-unstyled spaced">
                                            <li>
                                                <h2><strong>MOUSE LOGITECH </strong></h2>
                                            </li>

                                            <li>
                                                <i class="ace-icon fa fa-check bigger-110 green"></i> Unordered List Item # 2
                                            </li>

                                            <li>
                                                <i class="ace-icon fa fa-times bigger-110 red"></i> Unordered List Item # 3
                                            </li>
                                        </ul>

                                        <ul class="list-unstyled spaced2">
                                            <li>
                                                <i class="ace-icon fa fa-circle green"></i> Even more space
                                            </li>

                                            <li class="text-warning bigger-110 orange">
                                                <i class="ace-icon fa fa-exclamation-triangle"></i> Unordered List Item # 5
                                            </li>

                                            <li class="muted">
                                                <i class="ace-icon fa fa-angle-right bigger-110"></i> Unordered List Item # 6
                                            </li>

                                            <li>
                                                <ul class="list-inline">
                                                    <li>
                                                        <i class="ace-icon fa fa-share green bigger-110"></i> Inline List Item # 1
                                                    </li>
                                                    <li>List Item # 2</li>
                                                    <li>List Item # 3</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->

                        {{$product}}
                            <div class="col-xs-12 col-sm-9">
                                <h4 class="blue">
                                    <span class="middle">{{ strtoupper($product->name) }}</span>

                                    <span class="label label-purple arrowed-in-right">
                                        <i class="ace-icon fa fa-circle smaller-80 align-middle"></i>
                                        online
                                    </span>
                                </h4>

                                <div class="profile-user-info">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Description </div>

                                        <div class="profile-info-value">
                                            <span>{{ $product->description }}</span>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Location </div>

                                        <div class="profile-info-value">
                                            <i class="fa fa-map-marker light-orange bigger-110"></i>
                                            <span>Netherlands</span>
                                            <span>Amsterdam</span>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Stock </div>

                                        <div class="profile-info-value">
                                            <span>{{ $product->stock }}</span>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Updated Stock </div>

                                        <div class="profile-info-value">
                                            <span>{{ $product->updated_at }}</span>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Price </div>

                                        <div class="profile-info-value">
                                            <span>{{ 'Rp.'.number_format($product->price, '2', ',', '.') }}</span>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">

                                        <div class="profile-info-name"> Info Product </div>

                                        <!-- <div class="profile-info-value">
                                            <span>Rp. 200.000, 00</span>
                                        </div> -->
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Berat Gram</td>
                                                    <td>Kondisi</td>
                                                    <td>Asuransi</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <td>200 Gram</td>
                                                <td>Baru</td>
                                                <td>Opsional</td>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="hr hr-8 dotted"></div>

                                <div class="profile-user-info">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name"> Website </div>

                                        <div class="profile-info-value">
                                            <a href="#" target="_blank">www.product-com.com</a>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            <i class="middle ace-icon fa fa-facebook-square bigger-150 blue"></i>
                                        </div>

                                        <div class="profile-info-value">
                                            <a href="#">Find me on Facebook</a>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">
                                            <i class="middle ace-icon fa fa-twitter-square bigger-150 light-blue"></i>
                                        </div>

                                        <div class="profile-info-value">
                                            <a href="#">Follow me on Twitter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
</div>
@endsection