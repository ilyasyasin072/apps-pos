<div class="col-xs-12 col-sm-12">
    <div class="row">
        <div class="search-area well col-xs-12">
            <div class="pull-left">
                <b class="text-primary">Display</b> &nbsp;
                <div id="toggle-result-format" class="btn-group btn-overlap" data-toggle="buttons">
                    <label title="Thumbnail view" class="btn btn-lg btn-white btn-success active" data-class="btn-success" aria-pressed="true">
                        <input type="radio" value="2" autocomplete="off" />
                        <i class="icon-only ace-icon fa fa-th"></i>
                    </label>

                    <label title="List view" class="btn btn-lg btn-white btn-grey" data-class="btn-primary">
                        <input
                            type="radio"
                            value="1"
                            checked=""
                            autocomplete="off"
                        />
                        <i class="icon-only ace-icon fa fa-list"></i>
                    </label>

                    <label title="Map view" class="btn btn-lg btn-white btn-grey" data-class="btn-warning">
                        <input type="radio" value="3" autocomplete="off" />
                        <i class="icon-only ace-icon fa fa-crosshairs"></i>
                    </label>
                </div>
            </div>

            <div class="pull-right">
                <b class="text-primary">Order</b> &nbsp;
                <select id="category">
                    <option value="">--Selected--</option>
                </select>
            </div>
        </div>
    </div>
    <div class="scrollit-product">
        <div class="row" id="detail-show"></div>
    </div>
</div>

@push('js')
<script type="text/javascript">
    $(document).ready(function() {
        category();
    });

    function category() {
        $.ajax({
            url: "{{ route('category-json-api') }}",
            type: "GET",
            dataType: "JSON",
            success: function(e) {
                e.forEach((ev) => {
                    $("#category").append(
                        "<option value=" + ev.id + ">" + ev.name + "</option>"
                    );
                });
            },
        });
    }
</script>
@endpush