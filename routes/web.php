<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'AuthController@login')->name('auth.login');
Route::group(['prefix' => 'auth'], function () {
    Route::post('/check/login', 'AuthController@checkLogin')->name('auth.login.check');
    Route::post('/register', 'AuthController@register')->name('auth.register');
    Route::get('/', 'AuthController@logout')->name('auth.logout');
});

Route::group(['prefix' => 'web'], function () {
    // web customer views;
    Route::get('/dashboard', function () {
        return view('dashboard.index')->render();
    })->name('dashboard');

    Route::group(['prefix' => 'customer'], function () {
        Route::get('/', 'CustomerController@index')->name('customer-index');
        Route::get('/create', 'CustomerController@create')->name('customer-web-create');
        Route::get('/edit/{id}', 'CustomerController@edit')->name('customer-web-edit');
        Route::get('/update/{id}', 'CustomerController@update')->name('customer-web-update');
        Route::get('/store', 'CustomerController@store')->name('customer-web-store');
        Route::get('/show/{id}', 'CustomerController@show')->name('customer-web-show');
        Route::get('/delete/{id}', 'CustomerController@delete')->name('customer-web-delete');
        Route::post('/update/profile/{id}', 'CustomerController@updateProfile')->name('customer-update');
        Route::get('/reload/{id}', 'CustomerController@getAllJson')->name('customer-reload');
        Route::post('/import/customer', 'CustomerController@import')->name('import-customer');
    });
    // web category views;
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('category-web-index');
        Route::get('/create', 'CategoryController@create')->name('category-web-create');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('category-web-edit');
        Route::get('/update/{id}', 'CategoryController@update')->name('category-web-update');
        Route::get('/store', 'CategoryController@store')->name('category-store');
        Route::get('/show/{id}', 'CategoryController@show')->name('category-web-show');
        Route::get('/delete/{id}', 'CategoryController@delete')->name('category-delete');
    });
    // web product views;
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index')->name('product-web-index');
        Route::get('/create', 'ProductController@create')->name('product-web-create');
        Route::get('/edit/{id}', 'ProductController@edit')->name('product-web-edit');
        Route::get('/update/{id}', 'ProductController@update')->name('product-web-update');
        Route::get('/show/{id}', 'ProductController@show')->name('product-web-show');
        Route::post('/store', 'ProductController@store')->name('product-web-store');
        Route::post('/delete/{id}', 'ProductController@delete')->name('product-web-delete');
        Route::get('/detail', 'ProductController@productDetail')->name('product-detail');
        Route::get('/search/detail', 'ProductController@getProductSearch')->name('search-product');
        Route::get('/find','ProductController@find')->name('find-search');
        Route::get('/detail/{id}', 'ProductController@productDetailShow')->name('product-detail=show');
    });
    // web order views;
    Route::group(['prefix' => 'order'], function () {
        Route::get('/', 'OrderController@index')->name('order-web-index');
        Route::get('/create', 'OrderController@create')->name('order-web-create');
        Route::get('/edit/{id}', 'OrderController@edit')->name('order-web-edit');
        Route::get('/update/{id}', 'OrderController@update')->name('order-web-update');
        Route::get('/show/{id}', 'OrderController@show')->name('order-web-show');
        Route::get('/delete/{id}', 'OrderController@delete')->name('order-web-delete');
        Route::get('/export', 'OrderController@export')->name('order-web-export');
        Route::get('/export/view', 'OrderController@viewExport')->name('order-web-view');
        Route::post('/store', 'OrderController@store')->name('order-web-store');
        Route::get('/{id}/transaction', 'OrderController@transactionGet')->name('order-transaction');
        Route::get('/{id}/transaction/paid', 'OrderController@transactionGetPaid')->name('order-transaction-paid');
        Route::get('/update-total/{id}', 'OrderController@updateTotal')->name('order-total');
        Route::post('/post-total/{id}', 'OrderController@postTotal')->name('post-total');
        Route::get('/pending', 'OrderController@pendingOrder')->name('order-panding');
        Route::get('/pending/index', 'OrderController@pendingView')->name('order-panding-view');
        Route::get('/paid', 'OrderController@paidOrder')->name('order-paid');
        Route::get('/paid/index', 'OrderController@paidView')->name('order-panding-view');
        Route::post('/status/bayar/{id}', 'OrderController@returnMoney')->name('status-bayar');
        Route::get('/export/order', 'OrderController@downloadOrder')->name('order-download');
        Route::post('/import/order', 'OrderController@import')->name('import-order');
        Route::get('/transaction', 'OrderController@orderTransaction');
        Route::get('/select_data', 'OrderController@select_data');
    });
    // web order detail views;
    Route::group(['prefix' => 'orderdetail'], function () {
        Route::get('/', 'OrderDetailController@index')->name('orderdetail-web-index');
        Route::get('/create', 'OrderDetailController@create')->name('orderdetail-web-create');
        Route::get('/edit/{id}', 'OrderDetailController@edit')->name('orderdetail-web-edit');
        Route::get('/update/{id}', 'OrderDetailController@update')->name('orderdetail-web-update');
        Route::get('/show/{id}', 'OrderDetailController@show')->name('orderdetail-web-show');
        Route::post('/delete/{id}', 'OrderDetailController@delete')->name('orderdetail-web-delete');
        // Route::post('/store', 'OrderDetailController@store')->name('order.detail-store');
        Route::post('/store', 'OrderDetailController@CallOrderDetaiLogs')->name('order.detail-store');
        Route::post('/update/qty/{id}', 'OrderDetailController@updateQtyModal')->name('order-detail-modal');

        Route::get('/cookie/set','OrderDetailController@setCookie');
        Route::get('/cookie/get','OrderDetailController@getCookie');
    });

    // Pemakaian Except
    // Route::resource('/kategori', 'CategoryController')->except([
    //     'create', 'show'
    // ]);
});

Route::get('print', 'ProductController@print');
// Handle error
Route::get('notefound', function () {
    return view('errors.404')->render();
})->name('not_found');
