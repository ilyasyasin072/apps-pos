<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    // api customer json
    Route::group(['prefix' => 'customer'], function () {
        Route::get('/index-data', 'CustomerController@index_data')->name('customer-api');
        Route::post('/detail/{id}','CustomerController@GetCustomerId')->name('customer-detail-show');
    });
    // api product json
    Route::group(['prefix' => 'product'], function () {
        Route::get('/index-data', 'ProductController@index_data')->name('product-api');
        Route::get('/detail/{id}', 'ProductController@productDetailJson')->name('product-detail-show');
    });
    // api category json
    Route::group(['prefix' => 'category'], function () {
        Route::get('/index-data', 'CategoryController@index_data')->name('category-api');
        Route::get('/json', 'CategoryController@categorySelect')->name('category-json-api');
    });
    // api order json
    Route::group(['prefix' => 'order'], function () {
        Route::get('/index-data', 'OrderController@index_data')->name('order-api');
        Route::get('/json/export', 'OrderController@exportRange')->name('order-api-excel');
        Route::post('/customer-detail', 'OrderController@GetCustomer')->name('customer-detail-api');
        Route::post('/user-detail', 'OrderController@GetUser')->name('user-detail-api');
        // multiple search customer
        Route::post('/customer-multiple', 'OrderController@multipleCustomer')->name('customer-multiple');
        // multiple seach user
        Route::post('/user-multiple', 'OrderController@multipleUser')->name('user-multiple');
        // multiple Order
        Route::post('/product-json', 'OrderController@transaction')->name('product-json-api');
        Route::post('/product-multiple', 'OrderController@transactionMultiple')->name('product-multiple');
        Route::post('/product/detail', 'OrderController@productGetId')->name('product-detail-id');
        Route::post('/order-all/{id}', 'OrderController@paidSuccess')->name('order-all');
    });
    // api order detail json
    Route::group(['prefix' => 'orderdetail'], function () {
        Route::get('/index-data', 'OrderDetailController@index_data')->name('orderdetail-api');
        Route::post('/product-detail', 'OrderDetailController@GetProduct')->name('product-detail-api');
        Route::get('/order-detail', 'OrderDetailController@GetOrder')->name('order-detail-api');
        Route::post('/data/{id}', 'OrderDetailController@GetOrderDetailLogs')->name('order-trans-api');
        Route::post('/datafix/{id}', 'OrderDetailController@GetOrderDetail')->name('order-fix-api');

        Route::post('/update/qty/{id}', 'OrderDetailController@updateOrderDetail')->name('order-update-qty');
    });
});
