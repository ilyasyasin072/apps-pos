<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\OrderDetail;
use App\Order;
use App\Product;
use App\OrderDetailLogs;

class OrderDetailController extends Controller
{
    // json
    public function index_data()
    {
        $order_details = OrderDetail::with(['order', 'product', 'order.customer', 'order.user'])->get();
        return datatables()->of($order_details)
            ->addColumn('action', function ($order_detail) {
                return view('order_details._action', ['orderdetail' => $order_detail])->render();
            })
            ->make(true);
    }

    // views
    public function index()
    {
        return view('order_details.index')->render();
    }

    public function create()
    {
        return view('order_details.create')->render();
    }

    // store
    public function store(Request $request)
    {
        $format = \Request::format();
        $orderId = $request->input('order_id');
        $productId = $request->input('product_id');
        $qty = $request->input('qty');
        $price = $request->input('price');

        $order_details = new OrderDetail();

        $order_details->order_id = $orderId;
        $order_details->product_id = $productId;
        $order_details->qty = $qty;
        $order_details->price = $price;

        if ($order_details->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render =  response()->json([
                        'status' => 'success',
                        'order_id' => $orderId,
                        'product_id' => $productId,
                    ]);
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render =  response()->json([
                        'status' => 'errors',
                    ]);
                    break;
            }
        }

        return $render;
    }

    public function CallOrderDetaiLogs(Request $request)
    {
        $orderId = $request->input('order_id');
        $productId = $request->input('product_id');
        $qty = $request->input('qty');
        $price = $request->input('price');

        $sp = \DB::select('call sp_orderDetailLogs(' . $orderId . ', ' . $productId . ',' . $qty . ',' . $price . ')');
        return response()->json([
            'status' => 'success',
            'order_id' => $orderId,
            'product_id' => $productId,
        ]);
    }

    // edit
    public function edit(Request $request, $id)
    {
        $order_details = OrderDetail::GetId($id);
        return view(
            'order_details.edit',
            ['order_details' => $order_details]
        )->render();
    }

    // update
    public function update(Request $request, $id)
    {
        $format = \Request::format();
        $orderId = $request->input('order_id');
        $productId = $request->input('product_id');
        $qty = $request->input('qty');
        $price = $request->input('price');

        $order_details = OrderDetail::GetId($id);

        $order_details->order_id = $orderId;
        $order_details->product_id = $productId;
        $order_details->qty = $qty;
        $order_details->price = $price;

        if ($order_details->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render =  response()->json([
                        'status' => 'success',
                        'order_id' => $orderId,
                        'product_id' => $productId,
                    ]);
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render =  response()->json([
                        'status' => 'errors',
                    ]);
                    break;
            }
        }

        return $render;
    }

    // delete
    public function delete($id)
    {
        $format = \Request::format();
        $order_details = OrderDetailLogs::where('id', $id);
        if ($order_details->delete()) {
            switch ($format) {
                case 'html':
                default:
                    $render = response()->json(['success' => 'success'], 200);
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = response()->json(['danger' => $order_details], 400);
                    break;
            }
        }

        return $render;
    }

    public function GetProduct()
    {
        $product = Product::ProductData();

        try {
            if ($product->count() > 0) {
                return response()->json(['product' => $product], 200);
            }
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()], 500);
        }
    }

    public function GetOrder()
    {
        $order = Order::JsonOrder();
        try {
            if ($order->count() > 0) {
                return response()->json(['order' => $order], 200);
            }
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['errors' => $th->getMessage()], 500);
        }
    }

    public function GetOrderDetailLogs($id)
    {
        $orderDetail = OrderDetailLogs::JsonDetail($id);

        try {
            if ($orderDetail->count() > 0) {
                return response()->json(['order_detail' => $orderDetail], 200);
            }
            return response()->json(['order_detail' => $orderDetail], 200);
        } catch (\Throwable $th) {
            return response()->json(['order_detail' => 'Gagal'], 500);
        }
    }

    public function GetOrderDetail($id)
    {

        $orderDetail = OrderDetail::JsonDetail($id);

        try {
            if ($orderDetail->count() > 0) {
                return response()->json(['order_detail' => $orderDetail], 200);
            }
            return response()->json(['order_detail' => $orderDetail], 200);
        } catch (\Throwable $th) {
            return response()->json(['order_detail' => 'Gagal'], 500);
        }
    }

    public function updateOrderDetail(Request $request, $id)
    {
        $customer = OrderDetailLogs::findOrFail($id);
        $name = $request->get('name');
        $value = $request->get('value');
        try {
            if (in_array($name, ['qty'])) {
                $customer->$name = $value;
                if ($customer->save()) {
                    return json_encode(['success' => true, 'message' => 'Sukses!']);
                } else {
                    return json_encode(['success' => false, 'message' => 'Gagal disimpan!']);
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $vailed = 'check kembali barang';
            return response()->json($vailed, 500);
        }
    }

    public function setCookie()
    {
        $minutes = 1;
        $response = new Response('Hello World');
        $response->withCookie(cookie('name', 'virat', $minutes));
        return $response;
    }

    public function getCookie(Request $request)
    {
        $value = $request->cookie('name');
        echo $value;
    }

    public function updateQtyModal(Request $request, $id){
        $qty = $request->input('qty');
        // var_dump($qty); die;
        $orderDetail = OrderDetailLogs::GetId($id);
        $orderDetail->qty = $qty;
        $orderDetail->save();
        return response()->json(['orderDetail' => $orderDetail, 'massage' => 'success' ], 200);
    }
}
