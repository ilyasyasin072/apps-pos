<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Product;
use Carbon\Carbon;
use File;
use Illuminate\Support\Str;
use Image;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Neysi\DirectPrint\DirectPrint;

class ProductController extends Controller
{
    // json
    public function index_data()
    {
        // $product = Product::ForDatatable();
        $product = Product::with(['category'])->get();
        // return response()->json($product);
        return datatables()->of($product)
            ->editColumn('action', function ($product) {
                return view('products._action', ['product' => $product]);
            })
            ->editColumn('photo', function ($product) {
                return $product->photo ? '<img src="/uploads/product/' . $product->photo . '" alt="" width="50px" height="50px">' : '<a>tidak ada</a>';
            })
            ->rawColumns(['action', 'photo'])
            ->make(true);
    }

    // views
    public function index()
    {
        return view('products.index', ['menu' => 'Product'])->render();
    }

    public function create()
    {
        $date = Carbon::now()->timestamp;
        $generator_prodcutid = IdGenerator::generate(['table' => 'products', 'field' => 'code', 'length' => 22, 'prefix' =>'BRG/XXX/'.$date.'/']);
        return view('products.create', ['generator_prodcutid' => $generator_prodcutid])->render();
    }
    // store
    public function store(Request $request)
    {
        $format         = \Request::format();
        $code           = $request->input('code');
        $name           = $request->input('name');
        $description    = $request->input('description');
        $stock          = $request->input('stock');
        $price          = $request->input('price');
        $category_id    = $request->input('category_id');
        $file = $request->file('photo');

        $photo = null;
        if ($request->photo) {
            $photo = $this->saveFile($request->name, $request->photo);
        }
        $category       = new Product();

        $formatrp   = array("Rp.",".",",");
        $formatdb   = array("","",".");
        $replaced   = str_replace($formatrp,$formatdb,$price);

        $category->code = $code;
        $category->name = $name;
        $category->description = $description;
        $category->stock = $stock;
        $category->price = $replaced;
        $category->category_id = $category_id;

        $category->photo = $photo;


        if ($category->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()->route('product-web-index')->with('success', 'success!!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()->route('product-web-index')->with('danger', 'success!!');
                    break;
            }
        }

        return $render;
    }


    private function saveFile($name, $photo)
    {
        $images = Str::slug($name) . time() . '.' . $photo->getClientOriginalExtension();
        $path = public_path('uploads/product');

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        Image::make($photo)->save($path . '/' . $images);
        return $images;
    }

    // edit
    public function edit(Request $request, $id)
    {
        $product = Product::GetId($id);
        return view('products.edit', ['product' => $product])->render();
    }

    // update
    public function update(Request $request, $id)
    {
        $format = \Request::format();
        $code = $request->input('code');
        $name = $request->input('name');
        $description = $request->input('description');
        $stock = $request->input('stock');
        $price = $request->input('price');
        $category_id = $request->input('category_id');

        $category = Product::GetId($id);

        $formatrp   = array("Rp.",".",",");
        $formatdb   = array("","",".");
        $replaced   = str_replace($formatrp,$formatdb,$price);

        $category->code = $code;
        $category->name = $name;
        $category->description = $description;
        $category->stock = $stock;
        $category->price = $replaced;
        $category->category_id = $category_id;

        if ($category->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('product-web-index')
                        ->with('success', 'Product updated success');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('product-web-index')
                        ->with('danger', 'Product updated failed');
                    break;
            }
        }

        return $render;
    }

    // delete
    public function delete($id)
    {
        $format = \Request::format();
        $product = Product::GetId($id);
        try {
            if ($product->delete()) {
                switch ($format) {
                    case 'html':
                    default:
                        $render = response()->json(['status' => 'success', 'message' => 'berhasil']);
                        break;
                }
            } else {
                switch ($format) {
                    case 'html':
                    default:
                        $render = response()->json(['status' => 'danger', 'message' => 'gagal']);
                        break;
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {

            $render = response()->json(['status' => 'success', 'message' => 'Product tidak bisa di hapus Check Order Data'], 500);
        }

        return $render;
    }

    public function productDetail()
    {
        return view('products._each_detail')->render();
    }

    public function getProductSearch(Request $request)
    {
        $key = $request->key;

        $search = $request->search;


        if ($key != null) {
            $product = Product::Search($key);
            return response()->json(['product' => $product], 200);
        } else if ($search != null) {

            $category = Product::where('category_id', '=', $search)->get();
            return response()->json(['product' => $category], 200);
        }

        $data = Product::GetProduct();

        return response()->json(['product' => $data]);
    }

    public function productDetailJson($id) {
        $product = Product::with('category')->where('id', $id)->get();
        return response()->json(['product' => $product], 200);
    }

    public function productDetailShow($id) {

        $product = Product::with('category')->where('id', $id)->get();
        
        return view('products._product_detail', ['product' => $product[0]]);
    }

    public function print() {
        $file = '/home/ilyas/mewahapps/apps-pos/public/assets/file/invoice.pdf';
        $printerName = 'EPSON_L565_Series';
        
        $pdf = \App::make('dompdf.wrapper');
        
        $pdf->loadView('pdf.pdf')->save($file);
 
        // Get Printer
        $printers =  DirectPrint::getPrinters();

        $id = DirectPrint::printFile($file, $printerName);
        return response()->json('success');
    }
}
