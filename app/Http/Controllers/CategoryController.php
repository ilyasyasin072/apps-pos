<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Database\Eloquent\Collection;
use Validator;

class CategoryController extends Controller
{
    protected $category;

    public function __construct()
    {

        view()->share([
            'active_sidebar' => ['category', 'categoryd'],
            'breadcrumbs'    => [
                [
                    'url'  => '/web/category',
                    'text' => 'Category',
                ],
                [
                    'text' => 'Color Numbers',
                ],
            ],
        ]);
    }

    public function create()
    {
        $menus = 'Form Category';
        return view('categories.create', ['menus' => $menus])->render();
    }
    public function index_data()
    {
        $category = Category::ForDatatable();
        return datatables()->of($category)
            ->addColumn('action', function ($categoryd) {
                return view('categories._action', ['categoryd' => $categoryd])->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // views
    public function index()
    {
        $menus = 'Menus Category';
        return view('categories.index', ['menus' => $menus]);
    }

    // store
    public function store(Request $request)
    {
        $format = \Request::format();
        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($validatedData->fails()) {
            // $failed_json =  $validatedData->errors()->toJson();
            // $failed_decode = json_decode($failed_json);
            // $failed_array = (array) $failed_decode;
            // $error_name = $failed_array['name'][0];
            // $error_desc = $failed_array['description'][0];
            // $collection = collect([$error_name, $error_desc])->map(function ($name) {
            //     return strtoupper($name);
            // });
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        // ->withErrors([$error_name, $error_desc])
                        // ->withInput()
                        ->with('failed', 'Data Failed Check Again!!');
                    break;
            }
            return $render;
        }

        // if ($validatedData->is_valid()) {
        $format = \Request::format();
        $category_name = $request->input('name');
        $category_description = $request->input('description');

        $category = new Category();

        $category->name = $category_name;
        $category->description = $category_description;

        if ($category->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        ->with('success', 'Update Success!!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        ->with('warning', 'error');
                    break;
            }
        }
        return $render;
    }

    // edit
    public function edit($id)
    {
        $category = Category::ForGetId($id);
        return view('categories.edit', ['category' => $category])->render();
    }

    // update
    public function update(Request $request, $id)
    {
        $format = \Request::format();

        $validatedData = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);

        if ($validatedData->fails()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        ->withErrors($validatedData)
                        ->withInput()
                        ->with('failed', 'Data Tidak Boleh Kosong');
                    break;
            }
            return $render;
        }

        $name = $request->input('name');
        $desctipsion = $request->input('description');

        $category = Category::ForGetId($id);

        $category->name = $name;
        $category->description = $desctipsion;

        if ($category->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        ->with('success', 'update data category success!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index')
                        ->with('danger', 'fails data category danger!');
                    break;
            }
        }
        return $render;
    }

    // delete
    public function delete($id)
    {
        $format = \Request::format();
        $this->category =  Category::ForGetId($id);
        try {
                    
            if ($this->category != null) {
                $this->category->delete();
                switch ($format) {
                    case 'html':
                    default:
                        $render = redirect()
                            ->route('category-web-index')
                            ->with('success', 'Category telah dihapus!');
                        break;
                }
            } else {
                switch ($format) {
                    case 'html':
                    default:
                        $render = redirect()
                            ->route('category-web-index')
                            ->with('warning', 'error');
                        break;
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $render = redirect()
                            ->route('category-web-index')
                            ->with('warning', 'Product tidak bisa di hapus Check Order Data');
        }
        return $render;
    }

    public function show($id)
    {
        $format = \Request::format();
        try {
            $category = Category::ForGetId($id);
            return view('categories.show', ['category' => $category])->render();
        } catch (\Throwable $th) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('category-web-index');
                    break;
            }

            return $render;
        }
    }

    public function categorySelect() {
        $category = Category::all();
        return response()->json($category);
    }
}
