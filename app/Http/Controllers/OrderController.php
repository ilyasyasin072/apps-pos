<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Carbon\Carbon;
use App\Customer;
use App\User;
use App\Shared;
use App\Product;
use App\Exports\OrderExport;
use App\OrderDetail;
use App\Exports\OrderExcel;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\OrdersImport;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Auth;

class OrderController extends Controller
{
    // json
    public function index_data()
    {
        $order = Order::with(['customer', 'user'])->get();
        return datatables()->of($order)
            // ->editColumn('total', function($order) {
            //     return '<a href="#">Rp. number_format('.$order->total.', 0, ",", ".");</a>';
            // })
            ->addColumn('action', function ($order) {
                return view('orders._action', ['order' => $order])->render();
            })
            ->editColumn('status', function ($order) {
                return $order->status == 1 ? "<span class='badge badge-success'>Paid</span>" : "<span class='badge badge-warning'>Not Paid</span>";
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function create()
    {

        $statement = \DB::select("SHOW TABLE STATUS LIKE 'orders'");
        $nextId = $statement[0]->Auto_increment;
        return view('orders.create', ['nextId' => $nextId])->render();
    }

    // views
    public function index()
    {
        return view('orders.index')->render();
    }

    function invoiceNumber()
    {
        $latest = Order::latest()->first();

        if (!$latest) {
            return 'arm0001';
        }

        $string = preg_replace("/[^0-9\.]/", '', $latest->invoice_number);

        return 'arm' . sprintf('%04d', $string + 1);
    }
    // store
    public function store(Request $request)
    {
        $orders = Order::all();
        // $latest = Order::latest()->first();
        
        $latest = \DB::table('orders')->orderBy('invoice', 'desc')->first();

        $statement = \DB::select("SHOW TABLE STATUS LIKE 'orders'");
        $nextId = $statement[0]->Auto_increment;

        $format = \Request::format();
        $current_date_time = Carbon::now()->timestamp;
        // $invoice = $request->input('invoice');
        $customer = $request->input('customer_id');
        // $user = $request->input('user_id');
        $user = Auth::user()->id;
        $total = $request->input('total');

        $formatrp = array("Rp.", ".", ",");
        $formatdb = array("", "", ".");

        $replaced = str_replace($formatrp, $formatdb, $total);

        if ($orders->isEmpty()) {
            $invoice = 'INV' . $current_date_time . '0001';
        } else {

            if (!$latest) {
                return 'arm0001';
            }

            $string = preg_replace("/[^0-9\.]/", '', $latest->invoice);

            $invoice = 'INV' . sprintf('%4d', $string + 1);
            // $invoice = $string;
        }

        $order = new Order();

        $order->invoice = $invoice;
        $order->customer_id = $customer;
        $order->user_id = $user;
        $order->total = $replaced;

        try {
            if ($order->save()) {
                switch ($format) {
                    case 'html':
                    default:
                        $render =  response()->json([
                            'status' => 'success',
                        ], 200);
                        break;
                }
            } else {
                switch ($format) {
                    case 'html':
                    default:
                        $render =  response()->json([
                            'error' => 'gagal',
                        ], 500);
                        break;
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['error' => 'Invoice Sudah Tidak boleh Sama !!'], 500);
        }

        return $render;
    }

    // edit
    public function edit($id)
    {
        $order = Order::GetId($id);
        return view('orders.edit', ['order' => $order])->render();
    }

    // update
    public function update(Request $request, $id)
    {
    }

    // delete
    public function delete($id)
    {
        $format = \Request::format();
        $order = Order::GetId($id);

        try {

            if ($order->delete()) {
                switch ($format) {
                    case 'html':
                    default:
                        $render = response()->json(['success' => 'deleted'], 200);
                        break;
                }
            } else {
                switch ($format) {
                    case 'html':
                    default:
                        $render = response()->json(['error' => 'deleted'], 500);
                        break;
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['error' => 'Data Tidak bisa di hapus Check Order Detail !!'], 500);
        }

        return $render;
    }

    public function export()
    {
        return \Excel::download(new OrderExport, 'invoices.xlsx');
    }

    public function exportRange(Request $request)
    {
        if(request()->ajax()) {
            if ($request->from_date != '' && $request->to_date != '') {
                $fromdate       = Carbon::parse($request->from_date);
                $todate         = Carbon::parse($request->to_date);
                $from      = $fromdate->format('Y-m-d');
                $to        = $todate->format('Y-m-d');
                $data = Order::with(['user', 'customer'])->whereBetween('created_at', [$from, $to])->where('status', 1)->get();
            } else {
                $tglnow           = Carbon::now('Asia/Jakarta');
                $tglbsok          = Carbon::now()->addDays(20);
                $from     = $tglnow->format('Y-m-dY');
                $to = $tglbsok->format('Y-m-d');
                $value = 0;
                $data = Order::with(['customer', 'user'])->where('status', 1)->get();
            }
            $collection = collect($data);
            return datatables()::of($collection)
                ->editColumn('invoice', function ($order) {
                    return "<a href='{{ url('/web/order/".$order->id."/transaction/paid }}'>".$order->invoice."</a>";
                })
                ->editColumn('status', function ($order) {
                    return "<span class='badge badge-success'>Paid</span>";
                })
                ->rawColumns(['status', 'invoice'])
                ->make(true);
        }
    }

    public function downloadOrder(Request $request) {
            if ($request->from_date != '' && $request->to_date != '') {
                $fromdate       = Carbon::parse($request->from_date);
                $todate         = Carbon::parse($request->to_date);
                $from_date1      = $fromdate->format('Y-m-d');
                $to_date1        = $todate->format('Y-m-d');
            return Excel::download(new OrderExcel($from_date1, $to_date1), 'excelname.xlsx');
            }
    }

    public function viewExport()
    {
        return view('orders.excel')->render();
    }

    public function GetCustomer()
    {
        $customer = Customer::JsonCustomer();
        return response()->json(['customer' => $customer]);
    }

    public function GetUser()
    {
        $user = User::JsonUser();
        return response()->json(['user' => $user]);
    }

    public function multipleCustomer(Request $request)
    {
        $key = $request->key;


        $data =  Shared::multiexplode(array(",", "["), $key);
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] != "") {
            }
        }

        if (!empty($key)) {
            $customer = Customer::JsonCustomerId($data);
            if ($customer->count() > 0) {
                return response()->json(['customer' => $customer], 200);
            }
        } else {
            $customer = Customer::JsonCustomer();
            if ($customer->count() > 0) {
                return response()->json(['customer' => $customer], 200);
            }
        }

        $keyNull = $key == "null";
        if ($keyNull) {
            $customer = Customer::JsonCustomer();
            return response()->json(['customer' => $customer], 200);
        }
    }

    public function multipleUser(Request $request)
    {
        $key = $request->key;


        $data =  Shared::multiexplode(array(",", "["), $key);
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] != "") {
            }
        }

        if (!empty($key)) {
            $user = User::JsonJsonUserId($data);
            if ($user->count() > 0) {
                return response()->json(['user' => $user], 200);
            }
        } else {
            $user = User::JsonUser();
            if ($user->count() > 0) {
                return response()->json(['user' => $user], 200);
            }
        }

        $keyNull = $key == "null";
        if ($keyNull) {
            $user = User::JsonUser();
            return response()->json(['user' => $user], 200);
        }
    }

    public function show($id)
    {
        $customer = Customer::GetId($id);

        return view(
            'orders.show',
            [
                'customer' => $customer
            ]
        )->render();
    }

    public function transactionGet($id)
    {
        $order = Order::GetId($id);
        $order_customer = Order::with(['customer'])->where('id', $id)->get();
        $paid = OrderDetail::JsonDetail($id);
        return view('orders.transaction', ['order' => $order,  'paid' => $paid, 'customer_name' => $order_customer[0]->customer->name])->render();
    }

    public function transactionGetPaid($id)
    {
        $order = Order::GetId($id);
        $paid = OrderDetail::JsonDetail($id);
        $order_detail = OrderDetail::with(['order', 'product'])->where('order_id', $id)->get();
        return view('orders._each_paid', ['order' => $order, 'paid' => $paid[0], 'order_detail' => $order_detail])->render();
    }

    public function transaction()
    {
        // GetProduct() get all
        $product = Product::with(['category'])->get();

        if ($product->count() > 0) {
            return response()->json(['product' => $product], 200);
        }
        return response()->json(['product' => 'Product Kosong'], 400);
    }

    public function transactionMultiple(Request $request)
    {
        $key = $request->key;

        $data =  Shared::multiexplode(array(",", "["), $key);
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i] != "") {
            }
        }

        $product = Product::with(['category'])->whereIn('name', $data)->get();

        if ($product->count() > 0) {
            return response()->json(['product' => $product], 200);
        }

        $keyNull = $key == "null";

        if ($keyNull) {
            $product = Product::GetProduct();
            return response()->json(['product' => $product], 200);
        }

        $product = Product::GetProduct();
        return response()->json(['product' => $product], 200);
    }

    public function productGetId(Request $request)
    {
        $name = $request->key;

        try {
            $product = Product::with('category')->where('name', $name)->get();

            if ($product->count() > 0) {
                return response()->json(['product' => $product], 200);
            }
        } catch (\Throwable $th) {

            return response()->json(['product' => 'Product Kosong'], 400);
        }
    }

    public function updateTotal(Request $request, $id)
    {
        $order = Order::GetId($id);
        try {
            if ($order->count()) {
                return response()->json(['order' => $order], 200);
            }
        } catch (\Throwable $th) {
            return response()->json('failed', 500);
        }
    }

    public function postTotal(Request $request, $id)
    {
        $total = $request->get('total');
        $order = Order::GetId($id);

        $order->total = $total;
        // $order->status = 1;
        try {
            if ($order->save()) {
                $data =  \DB::select('call sp_orderDetail()');
                return response()->json(['order' => $order], 200);
            }
        } catch (\Throwable $th) {
            return response()->json(['order' => 'failed'], 500);
        }
    }

    public function paidSuccess($id)
    {
        $order_detail = Order::with(['customer'])->where('id', $id)->get();
        return response()->json(['order' => $order_detail], 200);
    }

    public function pendingView()
    {
        return view('orders.order_pending')->render();
    }

    public function paidView()
    {
        return view('orders.order_paid')->render();
    }

    public function pendingOrder()
    {
        $status = 0;
        $orderPending = Order::with(['customer', 'user'])->where('status', $status)->get();
        return datatables()->of($orderPending)
            // ->editColumn('total', function($order) {
            //     return '<a href="#">Rp. number_format('.$order->total.', 0, ",", ".");</a>';
            // })
            ->addColumn('action', function ($order) {
                return view('orders._action', ['order' => $order])->render();
            })
            ->editColumn('status', function ($order) {
                return $order->status == 0 && $order->total == 0 ? "<span class='badge badge-info'>Order Transaction</span>" : "<span class='badge badge-warning'>Panding Transaction</span>";
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function paidOrder()
    {
        $status = 1;
        $orderPending = Order::with(['customer', 'user'])->where('status', $status)->get();
        return datatables()->of($orderPending)
            // ->editColumn('total', function($order) {
            //     return '<a href="#">Rp. number_format('.$order->total.', 0, ",", ".");</a>';
            // })
            ->addColumn('action', function ($order) {
                return view('orders._action', ['order' => $order])->render();
            })
            ->editColumn('status', function ($order) {
                return  "<span class='badge badge-success'>Paid</span>";
            })
            ->rawColumns(['action', 'status'])
            ->make(true);
    }

    public function statusBayar($id)
    {
        $updateStatus = Order::where('id', $id);
        $updateStatus->status = 1;
        return response()->json(['status' => 'success'], 200);
    }

    public function returnMoney($id)
    {
        $total_bayar = request()->get('total_bayar');
        $returnMoney = Order::GetId($id);
        $returnMoney->return_money = $total_bayar;
        $returnMoney->status = 1;
        $returnMoney->save();

        return response()->json(['success' => $total_bayar], 200);
        // return redirect('/web/order/'.$id.'/transaction/paid');
    }

    public function import(Request $request) 
    {
        $this->validate($request, [
			'file' => 'required|mimes:xlsx'
        ]);

        		// menangkap file excel
        $file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_order',$nama_file);

        
        Excel::import(new OrdersImport, public_path('/file_order/'.$nama_file));
        // (new OrdersImport)->import(public_path('/file_order/'.$nama_file), null, \Maatwebsite\Excel\Excel::XLSX);
        
        return redirect('/web/orders')->with('success', 'All good!');
    }

    public function select_data(Request $request){
        $job_query = \DB::table('customers')->select(['id', 'name'])->whereRaw('name LIKE ?', [
            '%'.$request->input('q').'%'
        ]);
        $per_page = 30;
        $offset = ($request->input('page') - 1) * $per_page;
        return response()->json([
            'total_count' => $job_query->count(),
            'items'       => $job_query->skip($offset)->take($per_page)->get(),
        ]);
    }

    public function orderTransaction() {
        $id = IdGenerator::generate(['table' => 'transaction_log', 'length' => 10, 'prefix' =>'INV-']);
        return view('orders._order_transaction', ['product_id' => $id]);
    }
}
