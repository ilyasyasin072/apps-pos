<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;

class AuthController extends Controller
{
    // public function __construct()
    // {
    //     $this->redirectTo = url()->previous();
    //     $this->middleware('guest', ['except' => 'logout']);
    // }

    public function register(Request $request)
    {
        // $valid = Validator::make($request->all(), [
        //     'name'  => 'required|string|max:255',
        //     'email' => 'required|unique:users',
        //     'password' => 'required|min:6|confrimed',
        // ]);

        // if ($valid->fails()) {
        //     return response()->json(['error' => $valid->errors()->toJson()], 400);
        // }

        $name = $request->input('name');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        // dd($name, $email, $password);
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ]);



        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    public function login(Request $request)
    {
        return view('auth.login')->render();
    }

    public function checkLogin(Request $request)
    {
        $credentials = $request->only(['name', 'password']);
        // dd($credentials);
        try {
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'erros'], 400);
            }
            $user = auth()->user();
            if($user->status) {
                return response()->json(['users' => $user], 200);
            } else {
                return response()->json(['failed' => 'failed'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'erros'], 500);
        }
        
                    // $update = User::where('id', $user)->update(['_token' => $token]);
        
        // return redirect()->intended('web/dashboard');
    }

    public function logout()
    {
        \Session::flush();
        \Auth::logout();
        return redirect('/')->with('success', 'Logout Success');
    }
}
