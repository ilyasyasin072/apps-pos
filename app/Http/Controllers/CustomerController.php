<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CustomerImport;
class CustomerController extends Controller
{

    public function __construct() {
        // middle ware
    }

    protected $customers;
    // json

    public function create()
    {
        return view('customers.create')->render();
    }

    public function index_data()
    {
        $customer = Customer::ForDatatable();
        return datatables()->of($customer)
            // ->editColumn('id', function($customer){
            //     $count = (array) $customer->id;
            //     $da = count($count);
            //     for ($i = 1; $i <= 10; $i++) {
            //         echo $i;
            //     }
            // })
            ->editColumn('action', function ($customer) {
                return view('customers._action', ['customer' => $customer])->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    // views
    public function index()
    {
        return view('customers.index')->render();
    }

    // store
    public function store(Request $request)
    {
        $format     = \Request::format();
        $email      = $request->input('email');
        $name       = $request->input('name');
        $address    = $request->input('address');
        $phone      = $request->input('phone');

        $customers = new Customer();

        $customers->email   = $email;
        $customers->name    = $name;
        $customers->address = $address;
        $customers->phone   = $phone;


        if ($customers->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('customer-index')
                        ->with('success', 'Category telah disimpan!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('customer-index')
                        ->with('warning', 'error');
                    break;
            }
        }
        return $render;
    }

    // edit
    public function edit($id)
    {
        $customer = Customer::GetId($id);
        return view('customers.edit', ['customer' => $customer])->render();
    }

    // update
    public function update(Request $request, $id)
    {
        $format     = \Request::format();
        $email      = $request->input('email');
        $name       = trim($request->input('name'));
        $address    = $request->input('address');
        $phone      = $request->input('phone');

        $customers = Customer::GetId($id);

        $customers->email   = $email;
        $customers->name    = $name;
        $customers->address = $address;
        $customers->phone   = $phone;

        if ($customers->save()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect('web/customer/show/'.$id)
                        ->with('success', 'Customers telah disimpan!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('customer-index')
                        ->with('warning', 'error');
                    break;
            }
        }
        return $render;
    }

    // delete
    public function delete($id)
    {
        $format     = \Request::format();
        $this->customer   = Customer::GetId($id);

        if($this->customer->delete()) {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('customer-index')
                        ->with('success', 'Category telah dihapus!');
                    break;
            }
        } else {
            switch ($format) {
                case 'html':
                default:
                    $render = redirect()
                        ->route('customer-index')
                        ->with('danger', 'Failed!!!');
                    break;
            }
        }
        return $render;
    }

    public function show($id) {
        $customer = Customer::GetId($id);
        return view('customers.show', ['customer' => $customer, 'menu' => 'Show Customers'])->render();
    }

    public function updateProfile(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $name = $request->get('name');
        $value = $request->get('value');
        try {
            if ( in_array($name, ['name', 'email', 'address', 'phone']) ) {
                $customer->$name = $value;
                if ( $customer->save() ) {
                    return json_encode(['success' => true, 'message' => 'Sukses!']);
                }
                else {
                    return json_encode(['success' => false, 'message' => 'Gagal disimpan!']);
                }
            }
        }
        catch (\Illuminate\Database\QueryException $e) {
            $vailed = 'Email sudah ada, Silahkan check kembali';
            return response()->json($vailed, 500);
        }
    }

    public function getAllJson($id){
        $customer = Customer::findOrFail($id);
        return response()->json(['customer' => $customer], 200);
    }

    public function import(Request $request) 
    {
        $this->validate($request, [
			'file' => 'required|mimes:xlsx'
        ]);

        		// menangkap file excel
        $file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_order',$nama_file);

        
        Excel::import(new CustomerImport, public_path('/file_order/'.$nama_file));
        // (new OrdersImport)->import(public_path('/file_order/'.$nama_file), null, \Maatwebsite\Excel\Excel::XLSX);
        
        return redirect()->route('customer-index')->with('success', 'Upload Success');
    }


    public function GetCustomerId($id) {
        $customer = Customer::GetId($id);
        return response()->json($customer);
    }
    
}
