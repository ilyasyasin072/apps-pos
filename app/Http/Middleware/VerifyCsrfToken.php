<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'auth/*',
        'web/customer/*',
        'web/orderdetail/*',
        'web/product/*',
        'web/order/*',

        // api
        'v1/orderdetail/*',
    ];
}
