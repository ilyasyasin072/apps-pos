<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class Category extends Model
{
    protected $dates = ['deleted_at'];

    protected $fillabel = [
        'name',
        'description',
    ];

    // definition type data
    protected $casts = [
        'field' => 'boolean',
        'field' => 'float',
    ];

    protected $rules = [
        'name' => 'required|unique:categories',
        'description' => 'required',
    ];

    // validation atribute
    protected $validationAttributeNames = [
        'name' => 'Name Category',
        'description' => 'Name Category Description'
    ];

    // relational belongsTo , Hasone HasMany
    public function product() {
        return $this->hasmany(Product::class);
    }

    // Scope query in models
    public function scopeForDatatable($query) {
        return $query
            ->select([
               'id', 'name', 'description', 'created_at', 'updated_at'
            ])
            ->take('50');
    }
    
    // Scope where condition
    public function scopeForGetId($query, $id) {
        return $query
          ->where('id', $id)->first();
        //   ->orderBy('id', 'desc');
    }

    public function scopeJsonCategory($query) {
        return $query
            ->select(['id', 'name']);
    }

    public function scopeStandard($query) {
        return $query
          ->where('is_standard', '1')
          ->orderBy('id', 'asc');
      }

    public function scopeUpperName($query, $name) {
        return $query
            ->whereRaw('(UPPER("name") = ? OR UPPER("nama2") = ?)', [
                                strtoupper($name),
                                strtoupper($name),
                            ]);
    }
}
