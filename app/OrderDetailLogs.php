<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailLogs extends Model
{
    protected $fillabel = 'order_detail_logs';
    // PROCEDURE



// CREATE PROCEDURE sp_orderDetailLogs (IN order_idparam INT, IN product_idparam INT , IN qtyparam INT , IN priceparam varchar(225)) 
// BEGIN
//    INSERT INTO
//       order_detail_logs (order_id, product_id, qty, price) 
//    VALUES
//       (
//          order_idparam, product_idparam, qtyparam, priceparam
//       )
// ;
// END
// ;
// |
// DELIMITER ;



    public function scopeJsonDetail($query, $id) {
        return $query->select(['order_detail_logs.id as detail_id','order_detail_logs.*', 'products.*','orders.*','categories.*'])
        ->leftJoin('orders', 'orders.id', '=', 'order_detail_logs.order_id')
        ->leftJoin('products', 'products.id', '=', 'order_detail_logs.product_id')
        ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
        ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
        ->where('orders.id', $id)
        ->get();
    }

    public function scopeGetId($query, $id) {
        return $query->where('id', $id)->first();
    }

}
