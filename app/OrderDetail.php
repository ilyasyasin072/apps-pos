<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Product;
class OrderDetail extends Model
{
    // #1
    protected $fillabel = [
        'order_id',
        'product_id',
        'qty',
        'price ',
        'created_at',
    ];

    protected $orderDetail;

    // 
    public function order() {
        return $this->belongsTo(Order::class);
    } 

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function scopeForDatatable($query) {
        return $query
            ->select(['id', 'order_id', 'product_id', 'qty', 'price', 'created_at']);
    } 

    public function scopeCreateData($data) {
        $insert = \DB::table('order_details')->insert($data);
          return $insert;
    }

    public function scopeGetId($query, $id) {
        return $query->where('id', $id)->first();
    }

    public function scopeJsonDetail($query, $id) {
        return $query->select(['order_details.id as detail_id','order_details.*', 'products.*','orders.*','categories.*','customers.*', 'orders.updated_at as order_created', 'orders.total as total_amount','orders.return_money as return'])
        ->leftJoin('orders', 'orders.id', '=', 'order_details.order_id')
        ->leftJoin('products', 'products.id', '=', 'order_details.product_id')
        ->leftJoin('customers', 'customers.id', '=', 'orders.customer_id')
        ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
        ->where('orders.id', $id)
        ->get();
    }
}
