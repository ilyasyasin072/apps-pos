<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Customer;
use App\OrderDetail;

class Order extends Model
{
    protected $fillabel = ['invoice', 'customer_id', 'user_id', 'total', 'created_at', 'status', 'return_money'];
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function order_detail(){
        return $this->hasMany(OrderDetail::class);
    }

    public function scopeForExcel($query, $from, $to)
    {
        return $query
            ->whereBetween('created_at', [$from, $to])->get();
    }

    public function scopeGetId($query, $id)
    {
        return $query->where('id', $id)->first();
    }

    public function scopeJsonOrder($query)
    {
        return $query
            ->select(['id', 'invoice'])->get();
    }

    public function scopeStatusOrder($query, $id)
    {
        return $query->where('status', $id)->get();
    }

    public static function multiexplode ($delimiters,$string) {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
      }
      public function getDateAttribute($value)
      {
          return \Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
      }
      
      public function setDateAttribute($value)
      {
          $this->attributes['date'] = \Carbon::createFromFormat('d/m/Y', $value)->toDateString();
      }
}
