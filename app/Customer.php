<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
class Customer extends Model
{
    protected $guarded = [];
    protected $fillabel = [
        'id', 'name', 'email', 'address', 'phone', 'created_at'
    ];

    protected $rules = [
        'email'     => 'required|unique:customers',
        'address'   => 'required',
        'name'      => 'required',
        'phone'     => 'required'
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function scopeForDatatable($query)
    {
        return $query
            ->select([
                'id', 'name', 'email', 'address', 'phone', 'created_at'
            ]);
    }

    public function scopeGetId($query, $id)
    {
        return $query
            ->where('id', $id)->first();
    }

    public function scopeJsonCustomer($query)
    {
        return $query->select(['id', 'name as customer_name','email'])->get();
    }

    public static function multiexplode ($delimiters,$string) {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
      }

    public function scopeJsonCustomerId($query, $key)
    {
        return $query->select(['id', 'name as customer_name', 'email'])->whereIn('name', $key)->get();
    }
}
