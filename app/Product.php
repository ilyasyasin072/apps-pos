<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\OrderDetail;

class Product extends Model
{
    // #1 protected
    protected $guarded = [];
    // #2 protected fillabel
    protected $fillabel = [
        'name', 'description', 'stock', 'price', 'category_id', 'created_at', 'code', 'photo'
    ];
    // #3 protected rules
    protected $rules = [
        'name' => 'required',
        'description' => 'required',
        'stock' => 'required',
        'price' => 'required',
        'category_id' => 'required',
        'code' => 'required',
        'photo' => 'nullable|image|mimes:jpg,png,jpeg',
    ];
    // #4 protected validation
    protected $validationAttributeNames = [
        'name' => 'Name'
    ];
    // #5 scope datatable

    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function order_detail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function scopeForDatatable($query)
    {
        return $query
            ->select(['products.name as product_name', 'products.description', 'stock', 'price', 'code', 'photo', 'category_id', 'products.created_at', 'categories.name as category_name'])
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')->take(10);
    }

    public function scopeGetId($query, $id)
    {
        return $query
            ->where('id', $id)->first();
    }

    public function scopeProductData($query)
    {
        return $query
            ->select(['id', 'code as  code_product', 'name as name_product'])->get();
    }

    public function scopeGetProduct($query)
    {
        return $query->select(['*'])->get();
    }

    public function scopeGetMultipleData($query, $id)
    {
        return $query->whereIn('id', $id)->get();
    }

    public function scopeGetName($query, $id)
    {
        return $query->where('name', $id)->first();
    }

    public function scopeSearch($query, $key)
    {
        return $query->where('name', 'LIKE', "%{$key}%")->get();
    }

    public function scopeSearchCategory($query, $key)
    {
        return $query->where('category_id', '=', $key)->get();
    }
}
