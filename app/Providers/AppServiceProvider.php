<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $sidebar_menus = [
            'dashboard' => [
                'url'  => url('/web/dashboard/'),
                'icon' => 'fa fa-dashboard',
                'text' => 'Dashboard',
                'root' => true,
                'active' => 'active',
                'text-child' => 'Dashboard',
                'subs' => [],
            ],
            'customer' => [
                'url'        => '/customer',
                'icon'       => 'fa fa-user',
                'text'       => 'Customers',
                'active' => 'active',
                'text-child' => 'Management Customers',
                'root'       => true,
                'subs' => [
                    'customer' => [
                        'url'        => url('/web/customer/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Customer',
                        'subs' => []
                    ],
                    'customer_detail' => [
                        'url'        => url('/web/customer/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Customer Detail',
                        'subs' => []
                    ],
                ]
            ],
            'category' => [
                'url'        => '/category',
                'icon'       => 'fa fa-tag',
                'text'       => 'Categorys',
                'active' => 'active',
                'text-child' => 'Management Categorys',
                'root'       => true,
                'subs' => [
                    'category' => [
                        'url'        => url('/web/category/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Category',
                        'subs' => []
                    ],
                    'category_detail' => [
                        'url'        => url('/web/category/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Category Detail',
                        'subs' => []
                    ],
                ]
            ],
            'product' => [
                'url'        => '/product',
                'icon'       => 'fa fa-product-hunt',
                'text'       => 'Product',
                'text-child' => 'Management Product',
                'active' => 'active',
                'root'       => true,
                'subs' => [
                    'product' => [
                        'url'        => url('/web/product/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Product',
                        'subs' => []
                    ],
                    'product_detail' => [
                        'url'        => url('/web/product/detail'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Product Detail',
                        'subs' => []
                    ],
                ]
            ],
            'order' => [
                'url'        => '/order',
                'icon'       => 'fa fa-shopping-cart',
                'text'       => 'Order',
                'active'     => 'active',
                'root'       => true,
                'text-child'       => 'Management Order',
                'subs' => [
                    'order' => [
                        'url'        => url('/web/order/'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Order',
                        'subs' => []
                    ],
                    'order_panding' => [
                        'url'        => url('/web/order/pending/index'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Order Panding',
                        'subs' => []
                    ],
                    'order_paid' => [
                        'url'        =>  url('/web/order/paid/index'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Order Paid',
                        'subs' => []
                    ],
                ]
            ],
            'report' => [
                'url'        => '/report',
                'icon'       => 'fa fa-bar-chart',
                'text'       => 'Report',
                'text-child' => 'Report',
                'active' => 'active',
                'root'       => true,
                'subs' => [
                    'report' => [
                        'url'        => url('/web/order/export/view'),
                        'icon'       => 'fa fa-list-alt',
                        'text'       => 'Report',
                        'subs' => []
                    ],
                ]
            ],

        ];

        view()->share([
            'application_title'      => 'Apss',
            'sidebar_menus'          => $sidebar_menus,
            'active_sidebar'         => ['dashboard'],
            'breadcrumbs'            => [],
            'show_search'            => false,
            'show_checkbox_in_table' => true,
            'page_title'             => '',
            'default_layout'         => (\Request::ajax() ? 'layouts.ajax' : 'layouts.application'),
            'using_eModal'           => true,
        ]);

        // how to use @currency(params);

        Blade::directive('currency', function ($expression) {
            return "Rp. <?php echo number_format($expression, 0, ',', '.'); ?>";
        });

        Blade::component('components.card', 'card');
    }
}
