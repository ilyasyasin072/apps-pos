<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shared extends Model
{
    public static function multiexplode ($delimiters,$string) {
        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
      }
}
