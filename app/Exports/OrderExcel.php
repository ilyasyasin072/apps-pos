<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Support\Facades\DB;
    
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\Exportable;

class OrderExcel implements FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;  
    
        function __construct($from_date,$to_date) {
                $this->from_date = $from_date;
                $this->to_date = $to_date;
        }

        public function query()
        {
            return Order::query()->whereBetween('created_at', [$this->from_date, $this->to_date])->where('status', 1);
        }
    
}
