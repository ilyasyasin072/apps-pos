<?php

namespace App\Imports;

use App\Customer;
use Maatwebsite\Excel\Concerns\ToModel;

class CustomerImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'email'     => $row[0],
            'name'    => $row[1], 
            'address' => $row[2],
            'phone' => $row[3],
        ]);
    }
}
