<?php

namespace App\Imports;

use App\Order;
use Maatwebsite\Excel\Concerns\ToModel;

class OrdersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Order([
            'invoice'     => $row[0],
            'customer_id'    => $row[1], 
            'user_id' => $row[2],
            'total' => $row[3],
            'status' => $row[4],
            'return_money' => $row[5],
        ]);
    }
}
