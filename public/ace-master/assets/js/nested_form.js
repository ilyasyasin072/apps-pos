/*jslint
  browser: true, indent: 2, vars: true, unparam: true, sub: true, plusplus: true */
/*global
  jQuery, TableTools, ace, alert, NestedFormEvents, nestedFormEvents */
  (function ($) {
    'use strict';
    window.NestedFormEvents = function () {
      this.addFields = $.proxy(this.addFields, this);
      this.removeFields = $.proxy(this.removeFields, this);
    };
  
    NestedFormEvents.prototype = {
      addFields: function (e) {
        // Setup
        var link      = e.currentTarget;
        var assoc     = $(link).data('association');                // Name of child
        var blueprint = $('#' + $(link).data('blueprint-id'));
        var content   = blueprint.data('blueprint');       // Fields template
  
        // Make the context correct by replacing <parents> with the generated ID
        // of each of the parent objects
        var context = ($(link).closest('.fields').closestChild('input, textarea, select').eq(0).attr('name') || '').replace(/\[[a-z_]+\]$/, '');
  
        // If the parent has no inputs we need to strip off the last pair
        var current = content.match(new RegExp('\\[([a-z_]+)\\]\\[new_' + assoc + '\\]'));
        if (current) {
          context = context.replace(new RegExp('\\[' + current[1] + '\\]\\[(new_)?\\d+\\]$'), '');
        }
  
        // context will be something like this for a brand new form:
        // project[tasks_attributes][1255929127459][assignments_attributes][1255929128105]
        // or for an edit form:
        // project[tasks_attributes][0][assignments_attributes][1]
        if (context) {
          var parentNames = context.match(/[a-z_]+_attributes(?=\]\[(new_)?\d+\])/g) || [];
          var parentIds   = context.match(/[0-9]+/g) || [];
          var i;
  
          for (i = 0; i < parentNames.length; i++) {
            if (parentIds[i]) {
              content = content.replace(
                new RegExp('(_' + parentNames[i] + ')_.+?_', 'g'),
                '$1_' + parentIds[i] + '_'
              );
  
              content = content.replace(
                new RegExp('(\\[' + parentNames[i] + '\\])\\[.+?\\]', 'g'),
                '$1[' + parentIds[i] + ']'
              );
            }
          }
        }
  
        // Make a unique ID for the new child
        var regexp  = new RegExp('new_' + assoc, 'g');
        var new_id  = this.newId();
        content     = $.trim(content.replace(regexp, new_id));
  
        var field = this.insertFields(content, assoc, link);
        // bubble up event upto document (through form)
        field
          .trigger({ type: 'nested:fieldAdded', field: field })
          .trigger({ type: 'nested:fieldAdded:' + assoc, field: field });
        return false;
      },
      newId: function () {
        return new Date().getTime();
      },
      insertFields: function (content, assoc, link) {
        var target = $(link).data('target');
        if (target) {
          return $(content).appendTo($(target));
        }
        return $(content).insertBefore(link);
      },
      removeFields: function (e) {
        var $link = $(e.currentTarget),
          assoc = $link.data('association'); // Name of child to be removed
  
        var hiddenField = $link.prev('input[type=hidden]');
        hiddenField.val('1');
  
        var field = $link.closest('.fields');
        field.hide();
  
        field
          .trigger({ type: 'nested:fieldRemoved', field: field })
          .trigger({ type: 'nested:fieldRemoved:' + assoc, field: field });
        return false;
      }
    };
  
    window.nestedFormEvents = new NestedFormEvents();
    $(document)
      .delegate('form a.add_nested_fields',    'click', nestedFormEvents.addFields)
      .delegate('form a.remove_nested_fields', 'click', nestedFormEvents.removeFields);
  })(jQuery);
  
  // http://plugins.jquery.com/project/closestChild
  /*
   * Copyright 2011, Tobias Lindig
   *
   * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
   * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
   *
   */
  (function ($) {
    'use strict';
    $.fn.closestChild = function (selector) {
      // breadth first search for the first matched node
      if (selector && selector !== '') {
        var queue = [];
        queue.push(this);
        var node;
        var children;
        var i;
        var child;
        while (queue.length > 0) {
          node = queue.shift();
          children = node.children();
          for (i = 0; i < children.length; ++i) {
            child = $(children[i]);
            if (child.is(selector)) {
              return child; //well, we found one
            }
            queue.push(child);
          }
        }
      }
      return $();//nothing found
    };
  })(jQuery);
  
  // custom events
  jQuery(function ($) {
    'use strict';
  
    $(document).on('nested:fieldAdded', function (event) {
      var field = event.field;
  
      //select2
      field.find('.select2').select2({
        allowClear: true
      }).on('select2:select', function (evt) {
        var self = $(evt.target);
        var form = self.parents('form:eq(0)');
        var focusable = form.find('input,a,select,button,textarea,div[contenteditable=true]').filter(':visible');
  
        // Focus on the next field
        focusable.eq(focusable.index(self) + 1).focus();
      });
      function formatData(data) {
        return data.text;
      }
      function formatDataSelection(data) {
        return data.text;
      }
      field.find('.select2-ajax-data').select2({
        ajax: {
          dataType: 'json',
          delay: 250,
          data: function (params) {
            var query = {
              q: params.term, // search term
              page: params.page
            };
            var dependTo = $(this.context).data("dependTo");
            if (dependTo) {
              query["depended_id"] = $(dependTo).val();
            }
            return query;
          },
          processResults: function (data, params) {
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;
  
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 3,
        templateResult: formatData,
        templateSelection: formatDataSelection,
        allowClear: true
      }).on('select2:select', function (evt) {
        var self = $(evt.target);
        var form = self.parents('form:eq(0)');
        var focusable = form.find('input,a,select,button,textarea,div[contenteditable=true]').filter(':visible');
  
        // Focus on the next field
        focusable.eq(focusable.index(self) + 1).focus();
      });
  
      //date-picker
      field.find('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true,
        todayBtn: true
      }).next().on(ace.click_event, function (e) {
        $(e.currentTarget).prev().focus();
      });
      field.find('.date-picker').on('show.bs.modal', function (e) {
        e.stopPropagation();
      });
      field.find('.date-time-picker-1').datetimepicker().next().on(ace.click_event, function (e) {
        $(e.currentTarget).prev().focus();
      });
      field.find('.date-time-picker-1').on('show.bs.modal', function (e) {
        e.stopPropagation();
      });
  
      //autonumber
      field.find('.autodecimal').autoNumeric('init', {
        aSep: '.',
        aDec: ',',
        mDec: 6,
        aForm: false
      });
      field.find('.automoney').autoNumeric('init', {
        aSep: '.',
        aDec: ',',
        mDec: 2,
        aForm: false
      });
      field.find('.autointeger').autoNumeric('init', {
        aSep: '.',
        aDec: ',',
        mDec: 0,
        aForm: false
      });
    });
  
  });
  